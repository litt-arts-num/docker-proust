<div align="center">
  <img src="application/src/AppBundle/Resources/public/img/500px-Marcel_Proust_signature.svg.png" width="512">
  <h1>Corr-Proust</h1>
  <p>
		<b>Site pour le projet Corr-Proust</b>
	</p>
</div>

## Présentation générale
**Ce prototype permet** la mise en ligne d'une plate-forme d'édition et de diffusion de la correspondance de Marcel Proust. Développé dans le cadre du projet Corr-Proust de l'UMR Litt&Arts (Grenoble) porté par Françoise Leriche, l'outil permet :
- la mise en contexte (pages éditoriales modifiables directement en ligne),
- la gestion de comptes utilisateurs (avec différents niveaux de droits),
- la gestion du corpus de lettre (dépôt d'une lettre, création de collection et affectation de lettres à une ou plusieurs collections, publication d'une lettre),
- la visualisation des collections, liste des lettres et lettres (sous différentes version)

**Les lettres doivent respecter la DTD du projet** disponible ici : [DTD Corr Proust v8](https://gitlab.com/litt-arts-num/docker-proust/-/wikis/uploads/a30348bd35349be278ade62857182bcd/Corr_Proust_V8.dtd).

**De nombreuses évolutions sont à prévoir** dont le passage à un schéma TEI ([documentation officielle](https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/HD.html#HD44CD)), la conception et la création de visualisations non seulement sur les lettres, mais aussi de la correspondance en tant que telle : liens entre lettres, géolocalisation des expéditeurs/destinateurs, vues chronologiques, inscription de l'acte de correspondence dans son contexte qu'il soit lié à l'actualité de l'époque, à la vie de l'auteur ou à d'autres éléments de contexte, vues intégrant des outils de fouille de données et de traitements textuels pour permettre des études lexicales par exemple.

**En savoir plus sur le projet**
- [lejournal.cnrs.fr](https://lejournal.cnrs.fr/articles/les-lettres-de-proust-bientot-en-ligne)
- [univ-grenoble-alpes.fr](https://www.univ-grenoble-alpes.fr/fr/acces-direct/actualites/a-la-une/a-la-une-recherche/la-correspondance-de-marcel-proust-bientot-numerisee-271013.kjsp)
- [cahier.hypotheses.org](https://cahier.hypotheses.org/corr-proust)


## Installation
```
git clone git@gitlab.com:litt-arts-num/docker-proust.git
cd docker-proust
cp .env.dist .env
vi .env
# bien penser à répercuter les modifications dans app/config/parameters.yml si on ne garde pas les valeurs par défaut
sudo docker-compose up --build -d
sudo docker-compose exec apache make proust-init
```

## Mise à jour
```
git pull origin master
sudo docker-compose exec apache make proust-update
```

## Importer des lettres
* depuis les XML présents dans ```./application/var/data```
```
sudo docker-compose exec apache bash
bin/console corr:import
```

* en uploadant les fiches une par une via ```/admin/upload```.

 Cela nécessite d'avoir le rôle 'ROLE_ADMIN'
 Voici les commandes à lancer pour promote un compte
```
sudo docker-compose exec apache bash
bin/console fos:user:promote
```


## Commandes
* Supprimer la lettre qui a l'id LETTERID

```bin/console corr:delete-letter LETTERID```


* Supprimer toutes les lettres

```bin/console corr:delete-all-letters```

* Importer des lettres (depuis ```./application/var/data```)

```bin/console corr:import```


## Licence
[MIT](LICENSE)

## Crédits
<a title="By Morn [Public domain], from Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Marcel_Proust_signature.svg">Image By Morn [Public domain], from Wikimedia Commons</a>
