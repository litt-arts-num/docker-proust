<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EditorialContent
 *
 * @ORM\Table(name="editorial_content")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EditorialContentRepository")
 */
class EditorialContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contentfr", type="text", nullable=true)
     */
    private $contentFR;

    /**
     * @var string|null
     *
     * @ORM\Column(name="contenten", type="text", nullable=true)
     */
    private $contentEN;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return EditorialContent
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return EditorialContent
     */
    public function setContentFR($contentFR = null)
    {
        $this->contentFR = $contentFR;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContentFR()
    {
        return $this->contentFR;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return EditorialContent
     */
    public function setContentEN($contentEN = null)
    {
        $this->contentEN = $contentEN;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContentEN()
    {
        return $this->contentEN;
    }
}
