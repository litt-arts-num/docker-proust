<?php

namespace AppBundle\Command;

use AppBundle\Entity\EditorialContent;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;


class InitEditoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('corr-proust:init-edito')
            ->setDescription('init editorial content');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');

        $contentNames = ["homepage", "project", "team", "editorial_policy", "technical", "themes", "abbrevations", "bibliography", "use_guide", "thanks"];

        foreach ($contentNames as $contentName) {
          if (!$content = $em->getRepository(EditorialContent::class)->findByName($contentName)) {
              $content = new EditorialContent();
              $content->setName($contentName);
              $content->setContentFR(null);
              $content->setContentEN(null);
              $em->persist($content);
              $output->writeln('<info>Initialisation du contenu éditorial <comment>'.$contentName.'</comment></info>');

          }
        }

        $em->flush();
        $output->writeln('<info>Done.</info>');
    }
}
