<?php

namespace AppBundle\Controller\Edito;

use AppBundle\Entity\Edito\Update;
use AppBundle\Form\Type\Edito\UpdateType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UpdateController extends Controller
{

  /**
   * update form
   *
   * @Route("admin/edito/update/{id}", name="update_form", methods={"GET", "POST"}, defaults={"id"=null})
   * @ParamConverter("update", class="AppBundle:Edito\Update")
   */
    public function formAction(Update $update = null, Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        if (!$update) {
            $update = new Update;
        }

        $form = $this->createForm(UpdateType::class, $update);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($update);
            $em->flush();

            $this->addFlash('info', $this->get('translator')->trans('update_saved'));

            return $this->redirectToRoute('presentation', [
              'page' => 'updates',
          ]);
        }

        return $this->render('edito/presentation/updates-form.html.twig', [
          'update' =>  $update   ,
          'form' => $form->createView(),
       ]);
    }


    /**
     * update delete
     *
     * @Route("admin/edito/update/{id}/delete", name="update_delete", methods={"GET"})
     * @ParamConverter("update", class="AppBundle:Edito\Update")
     */
    public function delete(Update $update = null)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($update);
        $em->flush();

        $this->addFlash('info', $this->get('translator')->trans('update_deleted'));

        return $this->redirectToRoute('presentation', [
          'page' => 'updates',
        ]);
    }
}
