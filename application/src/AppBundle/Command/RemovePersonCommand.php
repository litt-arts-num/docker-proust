<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Finder\Finder;

class RemovePersonCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('corr-proust:delete-persons')
            ->setDescription('delete all persons');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');


        $persons = $em->getRepository('AppBundle:Person')->findAll();

        foreach ($persons as $person) {
            $em->remove($person);
            $output->writeln('<info>suppresion de la personne <comment>'.$person->getId().'</comment></info>');
        }

        $em->flush();
    }
}
