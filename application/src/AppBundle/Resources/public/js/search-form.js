jQuery(document).ready(function() {
  $(document).on('change', '.metadata-key', function() {
    formHandler.cleanOptions($(this));
  });
  $(document).on('click', '#add-criteria', function(event) {
    event.preventDefault();
    formHandler.addTagForm($collectionHolder);
  });
  $(document).on('click', '.remove-criteria', function(event) {
    event.preventDefault();
    $(this).closest(".criteria-container").remove();
  });

  var $collectionHolder;
  $collectionHolder = $('div.criterias');
  $collectionHolder.data('index', $collectionHolder.find(':input').length);
});

var formHandler = {
  addTagForm: function(collectionHolder) {
    var prototype = collectionHolder.data('prototype');
    var index = collectionHolder.data('index');
    var newForm = prototype;
    newForm = newForm.replace(/__name__/g, index);
    collectionHolder.data('index', index + 1);
    collectionHolder.append(newForm);
  },

  cleanOptions: function(select) {
    var selectKey = select;
    var selectValue = selectKey.parent().parent().find(".metadata-value");

    selectValue.prop("disabled", true);
    var id = selectKey.val();

    for (var i = 0; i < metadatas.length; i++) {
      currentKey = metadatas[i];
      currentId = currentKey.id
      if (currentId == id) {
        selectValue.empty();

        var opt = document.createElement('option');
        opt.value = "";
        opt.innerHTML = "-";
        opt.selected = "selected";
        selectValue.append(opt);

        for (var j = 0; j < currentKey.values.length; j++) {
          var currentValue = currentKey.values[j];
          var opt = document.createElement('option');
          opt.value = currentValue.id;
          opt.innerHTML = Translator.trans(currentValue.value);
          selectValue.append(opt);
        }
      }
    }
    selectValue.prop("disabled", false);
  }
};
