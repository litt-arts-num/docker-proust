var pages = {
    add: function(){
        var pages = $('#pages');
        var page = pages.data('prototype');
        page = page.replace(/__name__/g, $(".page").length);
        pages.append(page);
    },
    remove: function(btn){
        btn.closest('.card.page').remove();
    },
};
