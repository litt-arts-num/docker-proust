$(function() {

  var years = $(".year");
  var minYear = years.first().data("value");
  var maxYear = years.last().data("value");

  var min = new Date(minYear, 00, 01);
  var max = new Date(maxYear, 11, 31);

  var months = (maxYear - minYear + 1) * 12;
  var offset = months / 3;

  var minDefault = moment(min);
  minDefault.add(offset, "M");
  minDefault = minDefault.toDate();

  var maxDefault = moment(min);
  maxDefault.add(offset * 2, "M");
  maxDefault = maxDefault.toDate();


  var slider = $("#slider");

  slider.dateRangeSlider({
    bounds: {
      min: min,
      max: max
    },
    defaultValues: {
      min: minDefault,
      max: maxDefault
    },
    step: {
      days: 1
    },
    formatter: function(val) {
      var days = val.getDate(),
        month = val.getMonth() + 1,
        year = val.getFullYear();
      moment.locale(locale);

      return moment(year + "-" + month + "-" + days).format('LL');
    }
  });

  $(document).on('click', '#timeframe', function() {
    var dateValues = slider.dateRangeSlider("values");
    var from = moment(dateValues.min).format('YYYYMMDD');
    var to = moment(dateValues.max).format('YYYYMMDD');

    var route = Routing.generate('letter_timeframe', {
      'min': from,
      'max': to
    });
    window.location = route;
  });

})
