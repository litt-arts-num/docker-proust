<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\ORM\Mapping as ORM;

/**
 * MetadataKey.
 *
 * @ORM\Table(name="metadata_key")
 * @ORM\Entity()
 */
class MetadataKey
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="MetadataValue", mappedBy="key", cascade={"persist"})
     * @MaxDepth(0)
     */
    private $values;

    /**
     * @ORM\ManyToOne(targetEntity="MetadataGroup", inversedBy="keys", cascade={"persist"})
     * @MaxDepth(0)
     */
    private $group;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->values = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add value.
     *
     * @param \AppBundle\Entity\MetadataValue $value
     *
     * @return MetadataKey
     */
    public function addValue(\AppBundle\Entity\MetadataValue $value)
    {
        $this->values[] = $value;

        return $this;
    }

    /**
     * Remove value.
     *
     * @param \AppBundle\Entity\MetadataValue $value
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeValue(\AppBundle\Entity\MetadataValue $value)
    {
        return $this->values->removeElement($value);
    }

    /**
     * Get values.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return MetadataKey
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set group.
     *
     * @param \AppBundle\Entity\MetadataGroup|null $group
     *
     * @return MetadataKey
     */
    public function setGroup(\AppBundle\Entity\MetadataGroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group.
     *
     * @return \AppBundle\Entity\MetadataGroup|null
     */
    public function getGroup()
    {
        return $this->group;
    }
}
