<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Letter;
use AppBundle\Form\Type\UploadType;
use Symfony\Component\HttpFoundation\Request;

class LetterManager
{
    protected $formFactory;
    protected $em;
    protected $personManager;
    protected $pageManager;
    protected $XMLManager;
    protected $metadataManager;
    protected $session;

    public function __construct($formFactory, $em, $personManager, $pageManager, $XMLManager, $metadataManager, $session)
    {
        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->personManager = $personManager;
        $this->pageManager = $pageManager;
        $this->XMLManager = $XMLManager;
        $this->metadataManager = $metadataManager;
        $this->session = $session;
    }


    public function togglePublish($letter)
    {
        $isPublished = $letter->getPublished();
        $letter->setPublished(!$isPublished);

        $this->em->persist($letter);
        $this->em->flush();

        return !$isPublished;
    }


    public function handleXML(Request $request, $isSuperAdmin)
    {
        $form = $this->formFactory->createBuilder(UploadType::class)->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $data = $form->getData();
            $collections = $data['collections'];

            $xml = file_get_contents($data['xmlFile']->getPathname());

            return $this->importXML($xml, false, $collections, $isSuperAdmin);
        }
    }

    public function create($realId)
    {
        $letter = new Letter();
        $letter->setRealId($realId);

        return $letter;
    }

    public function getUploadForm()
    {
        $form = $this->formFactory->createBuilder(UploadType::class)->getForm()->createView();

        return $form;
    }

    public function getYears()
    {
        $years = [];
        $exactYears = $this->em->getRepository("AppBundle:Letter")->getYears();
        $intervalYears = $this->em->getRepository("AppBundle:Letter")->getYearsInterval();
        $boundaryYears = $this->em->getRepository("AppBundle:Letter")->getYearsBoundary();

        foreach ($exactYears as $y) {
            $years[] = intval($y["year"]);
        }

        foreach ($intervalYears as $interval) {
            $from = intval($interval["yearFrom"]);
            $to = intval($interval["yearTo"]);
            while ($from <= $to) {
                $years[] = $from;
                $from++;
            }
        }

        foreach ($boundaryYears as $boundary) {
            $from = $boundary["yearFrom"];
            $to = $boundary["yearTo"];

            if ($from !== null) {
                $years[] = intval($from);
            } else {
                $years[] = intval($to);
            }
        }

        sort($years);

        return array_unique($years);
    }



    public function importXML($xml, $fromCommand, $collections = null, $isSuperAdmin = false)
    {
        $crawler = $this->XMLManager->initCrawler($xml);

        $metadatas = [];

        /***** IDENTIFICATION LETTRE *****/
        $id = $this->XMLManager->extract($crawler, 'identifiant_cp', 'identifiant_num');
        /* Destinataire */
        $recipient = [];
        $destKey = $this->XMLManager->extract($crawler, 'destinataire', 'cle');
        $recipient['recipient_key'] = $destKey;
        $destName = $this->XMLManager->extract($crawler, 'destinataire', 'nom');
        $recipient['recipient_name'] = $destName;
        $destFirstname = $this->XMLManager->extract($crawler, 'destinataire', 'prenom');
        $recipient['recipient_firstname'] = $destFirstname;
        $destAdress = $this->XMLManager->extract($crawler, 'destinataire', 'adresse');
        $recipient['recipient_adress'] = $destAdress;
        $destInst = $this->XMLManager->extract($crawler, 'destinataire', 'institution');
        $recipient['recipient_institution'] = $destInst;
        $destSign = $this->XMLManager->extract($crawler, 'destinataire', 'signature');
        $recipient['recipient_signature'] = $destSign;
        $destRq = $this->XMLManager->extract($crawler, 'destinataire', 'remarque');
        $recipient['recipient_remarque'] = $destRq;

        $metadata_group = 'recipient';
        $metadatas[$metadata_group] = $recipient;
        $this->metadataManager->extract($metadata_group, $recipient);

        /* Expéditeur */
        $sender = [];
        $expKey = $this->XMLManager->extract($crawler, 'expediteur', 'cle');
        $sender['sender_key'] = $expKey;
        $expName = $this->XMLManager->extract($crawler, 'expediteur', 'nom');
        $sender['sender_name'] = $expName;
        $expFirstname = $this->XMLManager->extract($crawler, 'expediteur', 'prenom');
        $sender['sender_firstname'] = $expFirstname;
        $expAdress = $this->XMLManager->extract($crawler, 'expediteur', 'adresse');
        $sender['sender_adress'] = $expAdress;
        $expInst = $this->XMLManager->extract($crawler, 'expediteur', 'institution');
        $sender['sender_institution'] = $expInst;
        $expSign = $this->XMLManager->extract($crawler, 'expediteur', 'signature');
        $sender['sender_signature'] = $expSign;
        $expRq = $this->XMLManager->extract($crawler, 'expediteur', 'remarque');
        $sender['sender_remarque'] = $expRq;

        $metadata_group = 'sender';
        $metadatas[$metadata_group] = $sender;
        $this->metadataManager->extract($metadata_group, $sender);

        /* Redaction */
        $redaction = [];
        $dateRedCode = trim($this->XMLManager->extract($crawler, 'date_redaction', 'date_codee'));
        $redaction['writing_date_encoded'] = $dateRedCode;

        $dateRedLangage = $this->XMLManager->extract($crawler, 'date_redaction', 'date_langage');
        $redaction['writing_date_langagenat'] = $dateRedLangage;
        $dateRedCriteria = $this->XMLManager->extract($crawler, 'date_redaction', 'critere');
        $redaction['writing_date_criteria'] = $dateRedCriteria;
        $dateRedEntre1 = trim($this->XMLManager->extract($crawler, 'date_redaction', 'entre_le'));
        $redaction['writing_between1'] = $dateRedEntre1;
        $dateRedEntre2 = trim($this->XMLManager->extract($crawler, 'date_redaction', 'et_le'));
        $redaction['writing_between2'] = $dateRedEntre2;
        $dateRedRedate = $this->XMLManager->extract($crawler, 'date_redaction', 'lettre_redatee');
        $redaction['writing_redated'] = $dateRedRedate;
        $dateRedRq = $this->XMLManager->extract($crawler, 'date_redaction', 'remarque');
        $redaction['writing_remarque'] = $dateRedRq;
        $lieuRed = $this->XMLManager->extract($crawler, 'lieu_redaction', 'lieu');
        $redaction['writing_location'] = $lieuRed;
        $lieuRedCriteria = $this->XMLManager->extract($crawler, 'lieu_redaction', 'critere');
        $redaction['writing_location_criteria'] = $lieuRedCriteria;
        $lieuRedIndice = $this->XMLManager->extract($crawler, 'lieu_redaction', 'indice');
        $redaction['writing_location_clue'] = $lieuRedIndice;

        $metadata_group = 'writing';
        $metadatas[$metadata_group] = $redaction;
        $this->metadataManager->extract($metadata_group, $redaction);

        /* Envoi */
        $sending = [];
        $dateEnvoiCode = $this->XMLManager->extract($crawler, 'date_envoi', 'date_codee');
        $sending['sending_date_encoded'] = $dateEnvoiCode;
        $dateEnvoiLangage = $this->XMLManager->extract($crawler, 'date_envoi', 'date_langage');
        $sending['sending_date_langagenat'] = $dateEnvoiLangage;
        $dateEnvoiCriteria = $this->XMLManager->extract($crawler, 'date_envoi', 'critere');
        $sending['sending_date_criteria'] = $dateEnvoiLangage;
        $dateEnvoiEntre1 = $this->XMLManager->extract($crawler, 'date_envoi', 'entre_le');
        $sending['sending_between1'] = $dateEnvoiEntre1;
        $dateEnvoiEntre2 = $this->XMLManager->extract($crawler, 'date_envoi', 'et_le');
        $sending['sending_between2'] = $dateEnvoiEntre2;
        $dateEnvoiRedate = $this->XMLManager->extract($crawler, 'date_envoi', 'lettre_redatee');
        $sending['sending_redated'] = $dateEnvoiRedate;
        $dateEnvoiRq = $this->XMLManager->extract($crawler, 'date_envoi', 'remarque');
        $sending['sending_remarque'] = $dateEnvoiRq;
        $lieuEnvoi = $this->XMLManager->extract($crawler, 'lieu_envoi', 'lieu');
        $sending['sending_location'] = $lieuEnvoi;
        $lieuEnvoiCriteria = $this->XMLManager->extract($crawler, 'lieu_envoi', 'critere');
        $sending['sending_location_criteria'] = $lieuEnvoiCriteria;
        $lieuEnvoiIndice = $this->XMLManager->extract($crawler, 'lieu_envoi', 'indice');
        $sending['sending_location_clue'] = $lieuEnvoiIndice;

        $metadata_group = 'sending';
        $metadatas['sending'] = $sending;
        $this->metadataManager->extract($metadata_group, $sending);

        /* éditions antérieures*/
        $anteriors = [];
        $kolbEditions = $this->XMLManager->extract($crawler, 'kolb');
        foreach ($kolbEditions as $kolb) {
            $tempCrawler = $this->XMLManager->initCrawler($kolb);
            if ($numero_lettre = $this->XMLManager->extract($tempCrawler, 'kolb', 'numero_lettre')) {
                $page = $this->XMLManager->extract($tempCrawler, 'kolb', 'page');
                $tome = $this->XMLManager->extract($tempCrawler, 'kolb', 'tome');
                $kolb = ['type' => 'kolb', 'numero_lettre' => $numero_lettre, 'page' => $page, 'tome' => $tome];
                $anteriors[] = $kolb;

                $metadata_group = 'anterior';
                $this->metadataManager->extract($metadata_group, $kolb);
            }
        }

        $ouvrages = $this->XMLManager->extract($crawler, 'ouvrage');
        foreach ($ouvrages as $ouvrage) {
            $tempCrawler = $this->XMLManager->initCrawler($ouvrage);
            if ($titre1 = $this->XMLManager->extract($tempCrawler, 'ouvrage', 'titre1')) {
                $date_edition = $this->XMLManager->extract($tempCrawler, 'ouvrage', 'date_edition');
                $auteur = $this->XMLManager->extract($tempCrawler, 'ouvrage', 'auteur');
                $editeur = $this->XMLManager->extract($tempCrawler, 'ouvrage', 'editeur');
                $editeur_scient = $this->XMLManager->extract($tempCrawler, 'ouvrage', 'editeur_scient');
                $intégralite = $this->XMLManager->extract($tempCrawler, 'ouvrage', 'intégralite');
                $lieu_edition = $this->XMLManager->extract($tempCrawler, 'ouvrage', 'lieu_edition');
                $page = $this->XMLManager->extract($tempCrawler, 'ouvrage', 'page');

                $ouvrage = [
                  'type' => 'ouvrage',
                  'titre1' => $titre1,
                  'date_edition' => $date_edition,
                  'auteur' => $auteur,
                  'editeur' => $editeur,
                  'editeur_scient' => $editeur_scient,
                  'intégralite' => $intégralite,
                  'lieu_edition' => $lieu_edition,
                  'page' => $page
                ];

                $anteriors[] = $ouvrage;

                $metadata_group = 'anterior';
                $this->metadataManager->extract($metadata_group, $ouvrage);
            }
        }

        $lettersAnterior = $this->XMLManager->extract($crawler, 'lettres');
        foreach ($lettersAnterior as $letterAnterior) {
            $tempCrawler = $this->XMLManager->initCrawler($letterAnterior);
            if ($numero_lettre = $this->XMLManager->extract($tempCrawler, 'lettres', 'numero_lettre')) {
                $page = $this->XMLManager->extract($tempCrawler, 'lettres', 'page');
                $letterAnterior = ['type' => 'anterior_letter', 'numero_lettre' => $numero_lettre, 'page' => $page];
                $anteriors[] = $letterAnterior;
                $metadata_group = 'anterior';
                $this->metadataManager->extract($metadata_group, $letterAnterior);
            }
        }

        $cgs = $this->XMLManager->extract($crawler, 'cg');
        foreach ($cgs as $cg) {
            $tempCrawler = $this->XMLManager->initCrawler($cg);
            if ($numero_lettre = $this->XMLManager->extract($tempCrawler, 'cg', 'numero_lettre')) {
                $page = $this->XMLManager->extract($tempCrawler, 'cg', 'page');
                $tome = $this->XMLManager->extract($tempCrawler, 'cg', 'tome');
                $cg = ['type' => 'cg', 'numero_lettre' => $numero_lettre, 'page' => $page, 'tome' => $tome];
                $anteriors[] = $cg;
                $metadata_group = 'anterior';
                $this->metadataManager->extract($metadata_group, $cg);
            }
        }
        $metadatas['anteriors'] = $anteriors;

        /* edition_anterieure
        <revue/>
        <catalogue_expo/>
        <catalogue_vente/>
        */

        /* localisation_doc */
        $location = [];
        $loc = $this->XMLManager->extract($crawler, 'localisation_doc', 'localisation');
        $location['location_location'] = $loc;
        $locFonds = $this->XMLManager->extract($crawler, 'localisation_doc', 'fonds');
        $location['location_fund'] = $locFonds;
        $locCote = $this->XMLManager->extract($crawler, 'localisation_doc', 'cote');
        $location['location_pressmark'] = $locCote;
        $locCollection = $this->XMLManager->extract($crawler, 'localisation_doc', 'collection');
        $location['location_collection'] = $locCollection;
        $locInstitution = $this->XMLManager->extract($crawler, 'localisation_doc', 'institution');
        $location['location_institution'] = $locInstitution;
        $locCountry = $this->XMLManager->extract($crawler, 'localisation_doc', 'pays');
        $location['location_country'] = $locCountry;
        $locTown = $this->XMLManager->extract($crawler, 'localisation_doc', 'ville');
        $location['location_town'] = $locTown;
        $locDepartment = $this->XMLManager->extract($crawler, 'localisation_doc', 'departement');
        $location['location_departement'] = $locDepartment;

        $metadata_group = 'location';
        $metadatas['location'] = $location;
        $this->metadataManager->extract($metadata_group, $location);

        /***** DESCRIPTION PHYSIQUE *****/
        $physical = [];
        $integralite = $this->XMLManager->extract($crawler, 'proprietes_physiques', 'integralite_materielle');
        $physical['physical_integrality'] = $integralite;
        $lacune = $this->XMLManager->extract($crawler, 'proprietes_physiques', 'lacunes');
        $physical['physical_gap'] = $lacune;
        $dessin = $this->XMLManager->extract($crawler, 'proprietes_physiques', 'dessin');
        $physical['physical_drawing'] = $dessin;
        $nb_feuillets = $this->XMLManager->extract($crawler, 'proprietes_physiques', 'nombre_feuillets');
        $physical['physical_leaf_count'] = $nb_feuillets;
        $nbPages = $this->XMLManager->extract($crawler, 'proprietes_physiques', 'nombre_pages_ecrites');
        $physical['physical_written_pages_count'] = $nbPages;
        $width = $this->XMLManager->extract($crawler, 'proprietes_physiques', 'largeur');
        $physical['physical_width'] = $width;
        $height = $this->XMLManager->extract($crawler, 'proprietes_physiques', 'hauteur');
        $physical['physical_height'] = $height;
        $remark = $this->XMLManager->extract($crawler, 'proprietes_physiques', 'remarque');
        $physical['physical_remark'] = $remark;

        $metadata_group = 'physical';
        $metadatas['physical'] = $physical;
        $this->metadataManager->extract($metadata_group, $physical);

        $mains = $this->XMLManager->extract($crawler, 'main');
        $hands = [];
        foreach ($mains as $main) {
            $tempCrawler = $this->XMLManager->initCrawler($main);
            $nomScripteur = $this->XMLManager->extract($tempCrawler, 'main', 'nom_scripteur');
            $type = $this->XMLManager->extract($tempCrawler, 'main', 'type');
            $remarque = $this->XMLManager->extract($tempCrawler, 'main', 'remarque');
            $hand = ['hand_name' => $nomScripteur, 'hand_type' => $type, 'hand_remark' => $remarque];
            $hands[] = $hand;

            $metadata_group = 'hand';
            $this->metadataManager->extract($metadata_group, $hand);
        }
        $metadatas['hands'] = $hands;

        $sups = $this->XMLManager->extract($crawler, 'support');
        $supports = [];
        foreach ($sups as $sup) {
            $tempCrawler = $this->XMLManager->initCrawler($sup);
            $type = $this->XMLManager->extract($tempCrawler, 'support', 'type');
            $remarque = $this->XMLManager->extract($tempCrawler, 'support', 'remarque');
            $support = ['support_type' => $type, 'support_remark' => $remarque];
            $supports[] = $support;

            $metadata_group = 'support';
            $this->metadataManager->extract($metadata_group, $support);
        }
        $metadatas['supports'] = $supports;

        $paps = $this->XMLManager->extract($crawler, 'papier');
        $papers = [];
        foreach ($paps as $pap) {
            $tempCrawler = $this->XMLManager->initCrawler($pap);
            $color = $this->XMLManager->extract($tempCrawler, 'papier', 'couleur');
            $watermark = $this->XMLManager->extract($tempCrawler, 'papier', 'filigrane');
            $quality = $this->XMLManager->extract($tempCrawler, 'papier', 'qualite');
            $paper = ['paper_color' => $color, 'paper_watermark' => $watermark, 'paper_quality' => $quality];
            $papers[] = $paper;

            $metadata_group = 'paper';
            $this->metadataManager->extract($metadata_group, $paper);
        }
        $metadatas['papers'] = $papers;


        $contributions = $this->XMLManager->extract($crawler, 'contributeur');
        $contributers = [];
        foreach ($contributions as $contribution) {
            $tempCrawler = $this->XMLManager->initCrawler($contribution);
            $nom_contributeur = $this->XMLManager->extract($tempCrawler, 'contributeur', 'nom_contributeur');
            $date_contribution = $this->XMLManager->extract($tempCrawler, 'contributeur', 'date_contribution');
            $type_contribution = $this->XMLManager->extract($tempCrawler, 'contributeur', 'type_contribution');
            $contributer = ['nom_contributeur' => $nom_contributeur, 'date_contribution' => $date_contribution, 'type_contribution' => $type_contribution];
            $contributers[] = $contributer;

            $metadata_group = 'contributions';
            $this->metadataManager->extract($metadata_group, $contributer);
        }

        $metadatas['contributions'] = $contributers;

        /* document_associe */
        $additionalDocs = $this->XMLManager->extract($crawler, 'document_associe', 'existence');
        /* source_transcription */
        $transSource = $this->XMLManager->extract($crawler, 'source_transcription', 'source');


        if (!$letter = $this->em->getRepository('AppBundle:Letter')->findOneByRealId($id)) {
            $letter = $this->create($id);
        } elseif ($letter->getPublished() && !$isSuperAdmin) {
          $this->session->getFlashBag()->add('warning', "Seuls les administrateurs peuvent écraser une lettre déjà publiée");

          return false;
        }

        $letter->setPublished(false);

        /* DELETE PAGES */
        if ($pages = $letter->getPages()) {
            foreach ($pages as $page) {
                $this->em->remove($page);
            }
            $this->em->flush();
        }

        /* RECREATE PAGES */
        $pages = $this->XMLManager->extract($crawler, 'page');
        foreach ($pages as $page) {
            $this->pageManager->create($letter, $page);
        }

        $jsonStr = json_encode($metadatas, JSON_FORCE_OBJECT);
        $jsonObj = json_decode($jsonStr);


        $date = ($dateRedCode != '') ? \DateTime::createFromFormat('Ymd', $dateRedCode) : null;
        $dateFrom = $dateRedEntre1 ? \DateTime::createFromFormat('Ymd', $dateRedEntre1) : null;
        $dateTo = $dateRedEntre2 ? \DateTime::createFromFormat('Ymd', $dateRedEntre2) : null;

        if ($date === false | $dateFrom === false | $dateTo === false) {
            if ($fromCommand) {
                echo "problème date\n";
            } else {
                $this->session->getFlashBag()->add('warning', "problème date");
            }
            return false;
        }

        if (!$destKey | !$expKey) {
            if ($fromCommand) {
                echo "problème clé expéditeur ou destinataire \n";
            } else {
                $this->session->getFlashBag()->add('warning', "problème clé expéditeur ou destinataire");
            }
            return false;
        }


        $sender = $this->personManager->findByOrCreateByKey($expKey, $expName, $expFirstname);
        $recipient = $this->personManager->findByOrCreateByKey($destKey, $destName, $destFirstname);

        $letter->setSender($sender);
        $letter->setRecipient($recipient);
        $letter->setDate($date);
        $letter->setBetweenFrom($dateFrom);
        $letter->setBetweenTo($dateTo);

        $letter->setMetadatas($jsonObj);
        $letter->setTranscription($xml);

        $now = new \DateTime('now');
        if (!$letter->getCreationDate()) {
            $letter->setCreationDate($now);
        } else {
            $letter->setUpdateDate($now);
        }

        if ($letter->getCollections()) {
            foreach ($letter->getCollections() as $collection) {
                $collection->removeLetter($letter);
            }
        }

        foreach ($collections as $collection) {
            $collection->addLetter($letter);
        }

        $this->em->persist($letter);
        $this->em->flush();

        return $letter->getRealId();
    }


    public function delete(Letter $letter, $isSuperAdmin)
    {
        if ($isSuperAdmin || !$letter->getPublished()) {
          $this->em->remove($letter);
          $this->em->flush();

          $this->metadataManager->cleanOrphans();

          $this->session->getFlashBag()->add('info', "Lettre supprimée");
        }

        return;
    }
}
