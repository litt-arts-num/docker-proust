<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:elan="http://elan-numerique.fr"
    exclude-result-prefixes="xs elan" version="1.0">
    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>

    <!-- October 8, 2018 - AnneGF@CNRS - Creation -->

    <xsl:variable name="DEV">0</xsl:variable>
    <!-- Set to 0 or 1 for non DEV or DEV output -->
    <xsl:variable name="LINE_HEIGHT_FACTOR">200%</xsl:variable>
    <!-- Set lower to reduce the line height -->
    <xsl:variable name="ADD_DIV_FACTOR">20</xsl:variable>
    <!-- Set lower to reduce the space between add and inline following text -->
    <xsl:variable name="ADD_ROT_FACTOR">2</xsl:variable>
    <!-- Set lower to reduce the rotation for adds -->
    <xsl:variable name="ADD_UP_FACTOR">12</xsl:variable>
    <!-- Set lower to reduce the vertical shift -->


    <!-- PHP XSLT 2.0 compatibility not by default. We choose to stay with features available in XSLT 1.0 so no functions... -->
    <!--
    <xsl:function name="elan:capitalize-first" as="xs:string?" xmlns:elan="http://elan-numerique.fr"
        override-extension-function="yes">
        <xsl:param name="arg" as="xs:string?"/>
        <xsl:sequence select="concat(upper-case(substring($arg, 1, 1)), substring($arg, 2))"/>
    </xsl:function>
    -->

    <xsl:preserve-space elements="bloc"/>

    <xsl:template name="display_author">
        <xsl:param name="author"/>
        <xsl:choose>
            <xsl:when test="not(contains($author, ','))">
                <xsl:comment>Warning: The author is not well encoded "<xsl:value-of select="$author"/>" when expected format is "Last name, First name"</xsl:comment>
                <xsl:value-of select="$author"/>
            </xsl:when>
            <xsl:otherwise>
                <span class="first_name">
                    <xsl:value-of select="substring-after($author, ',')"/>
                </span>
                <xsl:text> </xsl:text>
                <span class="last_name">
                    <xsl:value-of select="substring-before($author, ',')"/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="nice-popover">
        <xsl:param name="content"/>
        <xsl:attribute name="data-trigger">
            <xsl:text>hover</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="data-html">
            <xsl:text>true</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="data-toggle">
            <xsl:text>popover</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="data-content">
            <xsl:value-of select="$content"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template name="add_default_title_and_class">
        <xsl:param name="append_to_title"/>
        <xsl:attribute name="class">
            <xsl:text>intervention_editeur </xsl:text>
            <xsl:value-of select="name(.)"/>
        </xsl:attribute>
        <xsl:attribute name="title">
            <!--If XSLT 2.0 : <xsl:value-of select="elan:capitalize-first(translate(name(.),'_',' '))"/>-->
            <xsl:value-of
                select="concat(translate(substring(name(.), 1, 1), 'abcdefghijklmnopqrstuvwxyz', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'), translate(substring(name(.), 2), '_', ' '))"/>
            <xsl:if test="string-length($append_to_title) > 0">
                <xsl:text> : </xsl:text>
                <xsl:value-of select="$append_to_title"/>
                <xsl:text/>
            </xsl:if>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="/">
        <style>
            .transcription_pseudodiplomatique .ajout_interlineaire_superieur {
            -ms-transform: skewY(-<xsl:copy-of select="$ADD_ROT_FACTOR"/>deg)
            translate(0px, -<xsl:copy-of select="$ADD_UP_FACTOR"/>px); /* IE 9 */
            -webkit-transform: skewY(-<xsl:copy-of select="$ADD_ROT_FACTOR"/>deg)
            translate(0px, -<xsl:copy-of select="$ADD_UP_FACTOR"/>px); /* Safari 3-8 */
            transform: skewY(-<xsl:copy-of select="$ADD_ROT_FACTOR"/>deg) translate(0px,-<xsl:copy-of select="$ADD_UP_FACTOR"/>px);
            }
            .transcription_pseudodiplomatique .ajout_interlineaire_inferieur {
            -ms-transform: skewY(<xsl:copy-of select="$ADD_ROT_FACTOR"/>deg)
            translate(0px, <xsl:copy-of select="$ADD_UP_FACTOR"/>px); /* IE 9 */
            -webkit-transform: skewY(<xsl:copy-of select="$ADD_ROT_FACTOR"/>deg)
            translate(0px, <xsl:copy-of select="$ADD_UP_FACTOR"/>px); /* Safari 3-8 */
            transform: skewY(<xsl:copy-of select="$ADD_ROT_FACTOR"/>deg) translate(0px,<xsl:copy-of select="$ADD_UP_FACTOR"/>px);
            }
            .transcription_pseudodiplomatique {
            line-height:<xsl:value-of select="$LINE_HEIGHT_FACTOR"/>;
            text-align: justify;
            }
            <xsl:if test="$DEV = 1">
                .transcription_pseudodiplomatique {
                    background-color: lightgrey;
                    padding: 5px;
                    margin: 3px;
                }
                .page_contenu {
                    background-color: grey;
                    border: 1px solid darkred;
                    padding: 5px;
                    margin: 3px;
                }
                .bloc {
                background-color: white;
                border: 1px solid darkgreen;
                padding: 5px;
                margin: 3px;
                }
                .col { background-color: orange; }
                .row { background-color: red; }
            </xsl:if>
        </style>
        <div class="content">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="lien_externe">
        <a>
            <xsl:attribute name="target">_blank</xsl:attribute>
            <xsl:attribute name="href">
                <xsl:value-of select="@URL"/>
            </xsl:attribute>
            <xsl:value-of select="."/>
        </a>
    </xsl:template>

    <xsl:template match="lien_interne">
        <a>
            <xsl:attribute name="href">
                <xsl:text>/letter/</xsl:text>
                <xsl:value-of select="@URL"/>
            </xsl:attribute>
            <xsl:value-of select="."/>
        </a>
    </xsl:template>

    <xsl:template match="exposant">
        <sup>
            <xsl:apply-templates/>
        </sup>
    </xsl:template>
</xsl:stylesheet>
