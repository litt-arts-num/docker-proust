$(".thumbnail").click(function() {
  var thumbnail = $(this);
  var pos = thumbnail.data("pos");
  $(".thumbnail-" + pos).removeClass("currentpage");
  var url = thumbnail.attr("data-full-size");
  thumbnail.addClass("currentpage");

  var viewer = (pos == 'left') ? viewerLeft : viewerRight;

  viewer.viewport.setRotation(0);
  viewer.open({
    type: 'image',
    url: url
  });
});

$('.note-call').click(function() {
  var noteModale = $("#noteModale");
  var id = $(this).data("note-id");
  var title = $("[data-note-title-id='" + id + "']").first().text();
  var text = $("[data-note-content-id='" + id + "']").first().html();
  noteModale.find(".modal-body").html(text);
  noteModale.find(".modal-title").html("Note " + title);
  noteModale.modal('show');
});

$('.reset-styles').click(function() {
  var pos = $(this).attr("data-pos");
  $("#nav-tabContent-" + pos + " mark").toggleClass("reset-styles");
});

const idLeft = 'seadragon-viewer-left'
const elLeft = document.getElementById(idLeft)
const urlLeft = elLeft.getAttribute('data-url');
var viewerLeft = OpenSeadragon({
  id: idLeft,
  showNavigator: false,
  showRotationControl: true,
  prefixUrl: '',
  zoomInButton: 'left-osd-zoom-in',
  zoomOutButton: 'left-osd-zoom-out',
  homeButton: 'left-osd-home',
  fullPageButton: 'left-osd-full-page',
  nextButton: 'left-osd-next',
  previousButton: 'left-osd-previous',
  rotateLeftButton: 'left-osd-left',
  rotateRightButton: 'left-osd-right',
  tileSources: {
    type: 'image',
    url: urlLeft
  }
});

const idRight = 'seadragon-viewer-right'
const elRight = document.getElementById(idRight)
const urlRight = elLeft.getAttribute('data-url');
var viewerRight = OpenSeadragon({
  id: idRight,
  showNavigator: false,
  showRotationControl: true,
  prefixUrl: '',
  zoomInButton: 'right-osd-zoom-in',
  zoomOutButton: 'right-osd-zoom-out',
  homeButton: 'right-osd-home',
  fullPageButton: 'right-osd-full-page',
  nextButton: 'right-osd-next',
  previousButton: 'right-osd-previous',
  rotateLeftButton: 'right-osd-left',
  rotateRightButton: 'right-osd-right',
  tileSources: {
    type: 'image',
    url: urlRight
  }
});
