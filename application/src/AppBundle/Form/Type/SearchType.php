<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('search', SubmitType::class, array(
          'attr' => [
            'class' => 'btn btn-secondary',
          ],
          'translation_domain' => 'messages',
          'label' => 'search',
        ));

        $builder->add('criterias', CollectionType::class, array(
          'entry_type' => CriteriaType::class,
          'entry_options' => array('label' => false),
          'allow_add' => true,
          'allow_delete' => true,
          'label' => 'criterias_list',
        ));
    }
}
