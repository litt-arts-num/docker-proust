<?php

namespace AppBundle\Manager;

use AppBundle\Form\Type\SearchType;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class SearchManager
{
    protected $em;
    protected $router;
    protected $formFactory;

    public function __construct($entityManager, $router, $formFactory)
    {
        $this->em = $entityManager;
        $this->router = $router;
        $this->formFactory = $formFactory;
    }

    public function convertCriterias($post)
    {
        $criterias = [];

        if (isset($post['criterias']) && $criteriaIds = $post['criterias']) {
            foreach ($criteriaIds as $criteriaId) {
                $key = $criteriaId['key'];
                $value = $criteriaId['value'];
                $key = $this->em->getRepository('AppBundle:MetadataKey')->find($key)->getName();
                $value = $this->em->getRepository('AppBundle:MetadataValue')->find($value)->getValue();
                $criterias[] = [$key, $value];
            }
        }

        return $criterias;
    }

    public function getForm()
    {
        return $form = $this->formFactory->createBuilder(SearchType::class, null, [
                'action' => $this->router->generate('search_submit'),
                'method' => 'POST',
              ])->getForm();
    }

    public function prepareMetadatas()
    {
        $metadatas = $this->em->getRepository('AppBundle:MetadataKey')->findAll();

        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getName();
        });
        $serializer = new Serializer(array($normalizer), array($encoder));

        return $serializer->serialize($metadatas, 'json');
    }
}
