<?php

namespace AppBundle\Form\Type\Edito;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UpdateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
          'attr' => ['class'=>'form-control'],
          'label' => 'title',
        ]);

        $builder->add('content', TextareaType::class, [
          'attr' => ['class'=>'form-control'],
          'label' => 'content',
        ]);

        $builder->add('save', SubmitType::class, array(
            'attr' => ['class' => 'btn btn-dark btn-sm'],
            'label' => 'save',
        ));
    }

    public function getName()
    {
        return 'update';
    }
}
