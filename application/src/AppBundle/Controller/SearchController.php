<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends Controller
{
    /**
     * @Route("/results", name="search_submit", methods={"POST"})
     */
    public function searchsubmitAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $post = $request->request->get('search');
        $criterias = $this->get('manager.search')->convertCriterias($post);

        $letters = $em->getRepository('AppBundle:Letter')->getByCriterias($criterias);

        return $this->render('search/results.html.twig', [
          'letters' => $letters,
          'criterias' => $criterias
        ]);
    }

    /**
     * @Route("letters/search", name="advanced_search", methods={"GET"})
     */
    public function searchAction()
    {
        $form = $this->get('manager.search')->getForm();
        $metadatas = $this->get('manager.search')->prepareMetadatas();

        return $this->render('search/form.html.twig', [
          'form' => $form->createView(),
          'metadatas' => $metadatas,
        ]);
    }
}
