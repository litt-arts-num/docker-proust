<?php

namespace AppBundle\Controller;

use AppBundle\Entity\EditorialContent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Form\Type\EditorialContentType;

class EditorialContentController extends Controller
{
    /**
     * Edit content
     *
     * @Route("/admin/edit-content/{id}", name="edit_content", methods={"GET", "POST"})
     */
    public function editAction(EditorialContent $content, Request $request)
    {
      if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          return new Response("Access Denied", 403);
      }

      $form = $this->createForm(EditorialContentType::class, $content);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          $em = $this->getDoctrine()->getManager();
          $em->persist($content);
          $em->flush();

          $this->addFlash('info', $this->get('translator')->trans('editorial_content_saved'));

          return $this->redirectToRoute('homepage');
      }

      return $this->render('editorial-content/edit.html.twig', ["form" => $form->createView()]);
    }

    /**
     * display contents
     *
     * @Route("/admin/editorial-contents", name="editorial_contents", methods={"GET"})
     */
    public function displayAction()
    {
      if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          return new Response("Access Denied", 403);
      }
      
      $em = $this->getDoctrine()->getManager();
      $contents = $em->getRepository(EditorialContent::class)->findAll();

      return $this->render('editorial-content/display.html.twig', ["contents" => $contents]);
    }
}
