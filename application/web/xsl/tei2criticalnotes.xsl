<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0">

    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>
    <!-- October 9, 2018 - AnneGF@CNRS - Creation-->

    <xsl:include href="common_tei2.xsl"/>
    <xsl:template match="courrier">
        <dl class="notes_critiques">
            <xsl:apply-templates/>
        </dl>
    </xsl:template>

    <!-- IGNORED -->
    <xsl:template match="courrier/descriptif"/>
    <xsl:template match="courrier/page/fichier_image"/>
    <xsl:template match="courrier/page/contenu/*[not(self::note_critique)]"/>

    <xsl:template name="get_note_number">
        <xsl:param name="identifiant"/>
        <xsl:value-of select="substring($identifiant, 2)"/><xsl:if
            test="not(substring($identifiant, 1, 1) = 'n') or not(number(substring($identifiant, 2)))">
            <sup>
                <xsl:attribute name="title">Identifiant de note erroné : "<xsl:value-of
                    select="$identifiant"/>"</xsl:attribute> ? </sup>
        </xsl:if>
    </xsl:template>
    <xsl:template name="notes" match="note_critique">
        <dt>
            <xsl:attribute name="id">
                <xsl:value-of select="@identifiant"/>
            </xsl:attribute>
            <xsl:attribute name="data-note-title-id"><xsl:value-of select="@identifiant"
                /></xsl:attribute>
            <xsl:value-of select="substring(@identifiant, 2)"/><xsl:if
                test="not(substring(@identifiant, 1, 1) = 'n') or not(number(substring(@identifiant, 2)))">
                <sup>
                    <xsl:attribute name="title">Identifiant de note erroné : "<xsl:value-of
                            select="@identifiant"/>"</xsl:attribute> ? </sup>
            </xsl:if>.
        </dt>
        <dd>
            <xsl:attribute name="data-note-content-id">
                <xsl:value-of select="@identifiant"/>
            </xsl:attribute>
            <xsl:apply-templates/> [<xsl:value-of select="@auteur"/>]
        </dd>
    </xsl:template>

    <xsl:template match="italique">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="rature">
        <s>
            <xsl:apply-templates/>
        </s>
    </xsl:template>
    <xsl:template match="personnage">
    <mark>
        <xsl:call-template name="add_default_title_and_class"/>
        <xsl:apply-templates/>
    </mark>
    </xsl:template>
    <xsl:template match="ajout">
        <div class="ajout" title="ajout" style="display:inline">
            <!--<span style="display: inline-block; transform:skewY(35deg) translate(-3px, -2px)">V</span>-->
            <span style="display: inline-block; transform:skewY(-2deg) translate(0, -12px)"><xsl:apply-templates/></span>
        </div>
    </xsl:template>
    <xsl:template match="renvoi">
        <xsl:choose>
            <xsl:when test="@type = 'note'">
                <a>
                    <xsl:attribute name="href">
                        <xsl:text>#</xsl:text>
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                    <xsl:text>note </xsl:text>
                    <xsl:call-template name="get_note_number">
                        <xsl:with-param name="identifiant"><xsl:value-of select="."/></xsl:with-param>
                    </xsl:call-template>
                </a>
            </xsl:when>
            <xsl:when test="@type = 'lettre'">
                <a>
                    <xsl:attribute name="href">
                        <xsl:text></xsl:text>
                        <xsl:value-of select="."/>
                    </xsl:attribute>
                    <xsl:text>lettre CP </xsl:text>
                    <xsl:value-of select="."/>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <span title="renvoi">
                    <xsl:value-of select="."/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
