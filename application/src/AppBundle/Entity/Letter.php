<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Letter.
 *
 * @ORM\Table(name="letter")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LetterRepository")
 */
class Letter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Page", mappedBy="letter", cascade={"persist", "remove"})
     */
    private $pages;

    /**
     * @ORM\ManyToOne(targetEntity="Person")
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity="Person")
     */
    private $recipient;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(name="creation_date", type="datetime", nullable=true)
     */
    private $creationDate;

    /**
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @ORM\Column(name="date_between_from", type="datetime", nullable=true)
     */
    private $betweenFrom;

    /**
     * @ORM\Column(name="date_between_to", type="datetime", nullable=true)
     */
    private $betweenTo;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $transcription;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $realId;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $metadatas;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published = false;

    /**
     * @ORM\ManyToMany(targetEntity="Collection", mappedBy="letters", cascade={"persist"})
     */
    protected $collections;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->pages = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add page.
     *
     * @param \AppBundle\Entity\Page $page
     *
     * @return Letter
     */
    public function addPage(\AppBundle\Entity\Page $page)
    {
        $this->pages[] = $page;

        return $this;
    }

    /**
     * Remove page.
     *
     * @param \AppBundle\Entity\Page $page
     */
    public function removePage(\AppBundle\Entity\Page $page)
    {
        $this->pages->removeElement($page);
    }

    /**
     * Get pages.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * Set sender.
     *
     * @param \AppBundle\Entity\Person $sender
     *
     * @return Letter
     */
    public function setSender(\AppBundle\Entity\Person $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender.
     *
     * @return \AppBundle\Entity\Person
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set recipient.
     *
     * @param \AppBundle\Entity\Person $recipient
     *
     * @return Letter
     */
    public function setRecipient(\AppBundle\Entity\Person $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient.
     *
     * @return \AppBundle\Entity\Person
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set date.
     *
     * @param \DateTime $date
     *
     * @return Letter
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set transcription.
     *
     * @param string $transcription
     *
     * @return Page
     */
    public function setTranscription($transcription)
    {
        $this->transcription = $transcription;

        return $this;
    }

    /**
     * Get transcription.
     *
     * @return string
     */
    public function getTranscription()
    {
        return $this->transcription;
    }

    /**
     * Get linearized.
     *
     * @return string
     */
    public function getLinearized()
    {
        $xslDoc = new \DOMDocument();
        $xslDoc->load('xsl/tei2linearized.xsl');

        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);

        return $proc->transformToXML(new \SimpleXMLElement($this->getTranscription()));
    }

    /**
     * Get pseudo-diplo.
     *
     * @return string
     */
    public function getPseudoDiplo()
    {
        $xslDoc = new \DOMDocument();
        $xslDoc->load('xsl/tei2pseudodiplo.xsl');

        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);

        return $proc->transformToXML(new \SimpleXMLElement($this->getTranscription()));
    }

    /**
     * Set realId.
     *
     * @param string $realId
     *
     * @return Letter
     */
    public function setRealId($realId)
    {
        $this->realId = $realId;

        return $this;
    }

    /**
     * Get realId.
     *
     * @return string
     */
    public function getRealId()
    {
        return $this->realId;
    }

    /**
     * Set metadatas.
     *
     * @param json $metadatas
     *
     * @return Letter
     */
    public function setMetadatas($metadatas)
    {
        $this->metadatas = $metadatas;

        return $this;
    }

    /**
     * Get metadatas.
     *
     * @return json
     */
    public function getMetadatas()
    {
        return $this->metadatas;
    }

    /**
     * Set betweenFrom.
     *
     * @param \DateTime|null $betweenFrom
     *
     * @return Letter
     */
    public function setBetweenFrom($betweenFrom = null)
    {
        $this->betweenFrom = $betweenFrom;

        return $this;
    }

    /**
     * Get betweenFrom.
     *
     * @return \DateTime|null
     */
    public function getBetweenFrom()
    {
        return $this->betweenFrom;
    }

    /**
     * Set betweenTo.
     *
     * @param \DateTime|null $betweenTo
     *
     * @return Letter
     */
    public function setBetweenTo($betweenTo = null)
    {
        $this->betweenTo = $betweenTo;

        return $this;
    }

    /**
     * Get betweenTo.
     *
     * @return \DateTime|null
     */
    public function getBetweenTo()
    {
        return $this->betweenTo;
    }


    public function getNotes()
    {
        $xslDoc = new \DOMDocument();
        $xslDoc->load('xsl/tei2criticalnotes.xsl');

        $proc = new \XSLTProcessor();
        $proc->importStylesheet($xslDoc);

        return $proc->transformToXML(new \SimpleXMLElement($this->getTranscription()));
    }

    /**
     * Set published.
     *
     * @param bool $published
     *
     * @return Letter
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published.
     *
     * @return bool
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set creationDate.
     *
     * @param \DateTime|null $creationDate
     *
     * @return Letter
     */
    public function setCreationDate($creationDate = null)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate.
     *
     * @return \DateTime|null
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set updateDate.
     *
     * @param \DateTime|null $updateDate
     *
     * @return Letter
     */
    public function setUpdateDate($updateDate = null)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate.
     *
     * @return \DateTime|null
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Add collection.
     *
     * @param \AppBundle\Entity\Collection $collection
     *
     * @return Letter
     */
    public function addCollection(\AppBundle\Entity\Collection $collection)
    {
        $this->collections[] = $collection;

        return $this;
    }

    /**
     * Remove collection.
     *
     * @param \AppBundle\Entity\Collection $collection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCollection(\AppBundle\Entity\Collection $collection)
    {
        return $this->collections->removeElement($collection);
    }

    /**
     * Get collections.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollections()
    {
        return $this->collections;
    }
}
