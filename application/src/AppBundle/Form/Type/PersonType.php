<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
          'attr' => ['class'=>'form-control'],
          'label' => 'person_name',
          'disabled' => true
        ]);

        $builder->add('personKey', TextType::class, [
          'attr' => ['class'=>'form-control'],
          'label' => 'person_key',
          'disabled' => true
        ]);

        $builder->add('displayName', TextType::class, [
          'attr' => ['class'=>'form-control'],
          'label' => 'person_displayName',
          'required' => false
        ]);

        $builder->add('save', SubmitType::class, array(
            'attr' => ['class' => 'btn btn-dark btn-sm'],
            'label' => 'save',
        ));
    }

    public function getName()
    {
        return 'person';
    }
}
