<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CollectionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
          'attr' => ['class'=>'form-control'],
          'label' => 'collection_name',
        ]);

        $builder->add('nameEN', TextType::class, [
          'attr' => ['class'=>'form-control'],
          'label' => 'collection_nameEN',
        ]);

        $builder->add('description', TextareaType::class, [
          'attr' => ['class'=>'form-control' ],
          'label' => 'collection_description',
        ]);

        $builder->add('descriptionEN', TextareaType::class, [
          'attr' => ['class'=>'form-control' ],
          'label' => 'collection_descriptionEN',
        ]);

        $builder->add('letters', EntityType::class, [
            'class' => 'AppBundle:Letter',
            'choice_label' => 'realId',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('l')
                          ->orderBy('l.realId', 'ASC');
            },
            'expanded'  => true,
            'multiple'  => true,
            'label' => 'collection_letters'
        ]);

        $builder->add('save', SubmitType::class, array(
            'attr' => ['class' => 'btn btn-dark btn-sm'],
            'label' => 'save',
        ));
    }

    public function getName()
    {
        return 'collection';
    }
}
