<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Letter;
use AppBundle\Entity\Person;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LetterController extends Controller
{
    /**
     * Display a letter depending on its proust ID
     *
     * @Route("/letter/{proustId}", name="letter_proustid", methods={"GET"})
     */
    public function displayByProustIdAction($proustId)
    {
        $em = $this->getDoctrine()->getManager();
        $letter = $em->getRepository('AppBundle:Letter')->findOneByRealId($proustId);
        if (!$letter || (!$letter->getPublished() && !$this->container->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))) {
            throw new \Exception();
        }

        return $this->render('letter/display.html.twig', ['letter' => $letter]);
    }

    /**
     * Display different subsets acess
     *
     * @Route("/letters/quick", name="access_quick", methods={"GET"})
     */
    public function quickAccessAction()
    {
        $em = $this->getDoctrine()->getManager();

        $years = $this->get("manager.letter")->getYears();
        $actors = $em->getRepository('AppBundle:Person')->getAll();
        $collections = $em->getRepository('AppBundle:Collection')->findAll();
        $recipients = $em->getRepository('AppBundle:Person')->getAllRecipients();
        $senders = $em->getRepository('AppBundle:Person')->getAllSenders();
        $actors = array_unique(array_merge($senders, $recipients), SORT_REGULAR);

        $out = [
          "years" => $years,
          'actors' => $actors,
          'recipients' => $recipients,
          'senders' => $senders,
          'collections' => $collections
        ];

        return $this->render('letter/access-quick.html.twig', $out);
    }

    /**
     * List letters by year
     *
     * @Route("/letters/y/{year}", name="letter_year", methods={"GET"})
     */
    public function getByYearAction($year)
    {
        $em = $this->getDoctrine()->getManager();
        $letters = $em->getRepository('AppBundle:Letter')->getByYear($year);

        return $this->render('letter/access-year.html.twig', [
          'year' => $year,
          'letters' => $letters
        ]);
    }

    /**
     * List letters by year and month
     *
     * @Route("/letters/y/{year}/m/{month}", name="letter_year_month", methods={"GET"})
     */
    public function getByYearAndMonthAction($year, $month)
    {
        $em = $this->getDoctrine()->getManager();
        $letters = $em->getRepository('AppBundle:Letter')->getByYearAndMonth($year, $month);

        return $this->render('letter/access-year-month.html.twig', [
          'year' => $year,
          'month' => $month,
          'letters' => $letters
        ]);
    }

    /**
     * List letters by sender
     *
     * @Route("/letters/sender/{personId}/", name="letter_sender", methods={"GET"})
     * @ParamConverter("person", class="AppBundle:Person", options={"id" = "personId"})
     */
    public function getBySenderAction(Person $person)
    {
        $em = $this->getDoctrine()->getManager();
        $letters = $em->getRepository('AppBundle:Letter')->getBySender($person);
        $title = $this->get('translator')->trans('letter_from_by', ['%sender%' => $person->getName()]);

        return $this->render('letter/list.html.twig', [
          'title' => $title,
          'letters' => $letters
        ]);
    }

    /**
     * @Route("/letters/recipient/{personId}/", name="letter_recipient", methods={"GET"})
     * @ParamConverter("person", class="AppBundle:Person", options={"id" = "personId"})
     */
    public function getByRecipientAction(Person $person)
    {
        $em = $this->getDoctrine()->getManager();
        $letters = $em->getRepository('AppBundle:Letter')->getByRecipient($person);
        $title = $this->get('translator')->trans('letter_from_to', ['%recipient%' => $person->getName()]);

        return $this->render('letter/list.html.twig', [
          'title' => $title,
          'letters' => $letters
        ]);
    }

    /**
     * @Route("/letters/person/{personId}/", name="letter_person", methods={"GET"})
     * @ParamConverter("person", class="AppBundle:Person", options={"id" = "personId"})
     */
    public function getByPersonAction(Person $person)
    {
        $em = $this->getDoctrine()->getManager();
        $letters = $em->getRepository('AppBundle:Letter')->getByPerson($person);
        $title = $this->get('translator')->trans('letter_from_with', ['%with%' => $person->getName()]);

        return $this->render('letter/list.html.twig', [
          'title' => $title,
          'letters' => $letters
        ]);
    }

    /**
     * @Route("/letters/all", name="letter_all", methods={"GET"})
     */
    public function getAllAction()
    {
        $em = $this->getDoctrine()->getManager();
        $letters = $em->getRepository('AppBundle:Letter')->findBy(['published' => true], ['realId' => 'ASC']);
        $title = $this->get('translator')->trans('letter_from_all');

        return $this->render('letter/list.html.twig', [
          'title' => $title,
          'letters' => $letters
        ]);
    }

    /**
     * @Route("/letters/random", name="letter_random", methods={"GET"})
     */
    public function getRandomAction()
    {
        $letters = $this->getDoctrine()->getManager()->getRepository('AppBundle:Letter')->findByPublished(true);
        shuffle($letters);

        return $this->redirectToRoute('letter_proustid', [
          'proustId' => $letters[0]->getRealId(),
        ]);
    }

    /**
     * @Route("/letters/from/{min}/to/{max}", name="letter_timeframe", methods={"GET"}, options = { "expose" = true })
     */
    public function getByTimeFramedAction($min, $max)
    {
        $em = $this->getDoctrine()->getManager();
        $min = new \DateTime($min);
        $max = new \DateTime($max);
        $letters = $em->getRepository('AppBundle:Letter')->getByTimeFrame($min, $max);

        return $this->render('letter/access-timeframe.html.twig', [
          'min' => $min,
          'max' => $max,
          'letters' => $letters
        ]);
    }

    /**
     * @Route("/letters/word", name="word_access", methods={"POST"})
     */
    public function getByWordAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $word = $request->request->get('word-access-input');
        // $letters = $em->getRepository('AppBundle:Letter')->getByWord($word);
        // solution horrible en attendant de passer le site en TEI
        $letters = $em->getRepository('AppBundle:Letter')->findByPublished(true);
        $matchingLetters = [];
        foreach ($letters as $letter) {
          $xml = $letter->getTranscription();
          $xml = preg_replace("/<descriptif.*?<\/descriptif>/s", "", $xml);
          $text = strip_tags($xml);
          if (strpos($text, $word) !== false) {
            $matchingLetters[] = $letter;
          }
        }

        $title = $this->get('translator')->trans('access_word_request', ['%word%' => $word]);

        return $this->render('letter/list.html.twig', [
          'letters' => $matchingLetters,
          'title' => $title,
          'word' => $word
        ]);
    }

    /**
     * @Route("/letters/proust-id", name="proustid_access", methods={"POST"})
     */
    public function getByProustIdAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->request->get('proust-access-input');

        if ($letter = $em->getRepository('AppBundle:Letter')->findOneByRealId($id)) {
            return $this->redirectToRoute('letter_proustid', [
              'proustId' => $letter->getRealId(),
            ]);
        }

        $this->addFlash('info', $this->get('translator')->trans('no_letter_found_for_id', ['%id%'=>$id]));

        return $this->redirectToRoute('access_quick');
    }

    /**
     * @Route("/admin/upload", name="upload_form", methods={"GET"})
     */
    public function uploadFormAction()
    {
        $form = $this->get('manager.letter')->getUploadForm();

        return $this->render('letter/upload.html.twig', [
           'form' => $form,
       ]);
    }


    /**
     * @Route("/admin/upload", name="upload_submit", methods={"POST"})
     */
    public function uploadAction(Request $request)
    {
        $isSuperAdmin = $this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN');
        $realId = $this->get('manager.letter')->handleXML($request, $isSuperAdmin);

        if (!$realId) {
            return $this->redirectToRoute('upload_form');
        }

        $msg = ($isSuperAdmin) ? 'letter_loaded_but_not_published' : 'letter_loaded';
        $this->addFlash('info', $this->get('translator')->trans($msg));
        $this->addFlash('warning', $this->get('translator')->trans('sharepoint_notice'));

        return $this->redirectToRoute('letter_proustid', ['proustId' => $realId]);
    }

    /**
     * (un)publish a letter depending on its proust ID
     *
     * @Route("/admin/publish/letter/{proustId}", name="publish_toggle_letter", methods={"GET"})
     */
    public function publishToggleAction($proustId)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        $em = $this->getDoctrine()->getManager();
        $letter = $em->getRepository('AppBundle:Letter')->findOneByRealId($proustId);
        if (!$letter) {
            throw new \Exception();
        }

        $isPublished = $this->get("manager.letter")->togglePublish($letter);
        $msg = $isPublished ? "letter_published" : "letter_unpublished";
        $this->addFlash('success', $this->get('translator')->trans($msg));

        return $this->redirectToRoute('letter_proustid', ['proustId' => $proustId]);
    }

    /**
     *
     * @Route("/admin/letters/unpublished", name="unpublished_letters", methods={"GET"})
     */
    public function unpublishedLettersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $letters = $em->getRepository('AppBundle:Letter')->findByPublished(false);
        $title = $this->get('translator')->trans('unpublished_letters');

        return $this->render('letter/list.html.twig', [
          'title' => $title,
          'letters' => $letters
        ]);
    }


    /**
     * Delete a letter depending on its proust ID
     *
     * @Route("/admin/delete/letter/{proustId}", name="delete_letter", methods={"GET"})
     */
    public function deleteAction($proustId)
    {
        $isSuperAdmin = $this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN');

        $em = $this->getDoctrine()->getManager();
        $letter = $em->getRepository('AppBundle:Letter')->findOneByRealId($proustId);
        if (!$letter) {
            throw new \Exception();
        }

        $this->get("manager.letter")->delete($letter, $isSuperAdmin);

        return $this->redirectToRoute('homepage');
    }
}
