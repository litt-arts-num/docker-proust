<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\Type\EditorialContentType;

class EditorialContentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
          'attr' => ['class'=>'form-control'],
          'label' => 'edito_name',
          'disabled' => true
        ]);

        $builder->add('contentFR', TextareaType::class, [
          'attr' => ['class'=>'form-control tinymce', 'data-theme' => 'advanced'],
          'label' => 'contentFR',
        ]);

        $builder->add('contentEN', TextareaType::class, [
          'attr' => ['class'=>'form-control tinymce', 'data-theme' => 'advanced'],
          'label' => 'contentEN',
        ]);

        $builder->add('save', SubmitType::class, array(
            'attr' => ['class' => 'btn btn-dark btn-sm'],
            'label' => 'save',
        ));
    }

    public function getName()
    {
        return 'editorial_content';
    }
}
