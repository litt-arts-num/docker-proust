<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class RemoveLetterCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('corr-proust:delete-letter')
            ->setDescription('delete a letter')
            ->addArgument('letterId', InputArgument::REQUIRED, 'letter to delete');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $letterId = $input->getArgument('letterId');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');

        if (!$letter = $em->getRepository('AppBundle:Letter')->findOneByRealId($letterId)) {
            $output->writeln('<info>la lettre existe pas</info>');
            return;
        }

        $em->remove($letter);
        $em->flush();
        $output->writeln('<info>suppresion de la lettre <comment>'.$letterId.'</comment></info>');
    }
}
