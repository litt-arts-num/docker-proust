<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0">

    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>
    <!-- October 8, 2018 - AnneGF@CNRS - Creation -->

    <!--<xsl:include href="common_tei2.xsl"/>-->
    <xsl:template match="courrier">
        <div class="transcription_linearisee">
            <xsl:apply-templates/>
            <!-- Show postscriptum after other elements. First the postscriptum at the end of the letter (having no bloc 'texte' after), then the other postscriptum -->
            <xsl:for-each
                select="page/contenu/bloc[@type_bloc = 'postscriptum' and (count(following-sibling::bloc[@type_bloc = 'texte']) = 0)]">
                <!--<xsl:sort select="position()" data-type="number" order="ascending"/>-->
                <xsl:copy>
                    <xsl:apply-templates select="." mode="show"/>
                </xsl:copy>
            </xsl:for-each>
            <xsl:for-each
                select="page/contenu/bloc[@type_bloc = 'postscriptum' and (count(following-sibling::bloc[@type_bloc = 'texte']) > 0)]">
                <!--<xsl:sort select="position()" data-type="number" order="ascending"/>-->
                <xsl:copy>
                    <xsl:apply-templates select="." mode="show"/>
                </xsl:copy>
            </xsl:for-each>
        </div>
    </xsl:template>

    <xsl:include href="common_tei2transcriptions.xsl"/>

    <!-- Transcription processing for linearized view -->
    <xsl:template match="courrier/page/contenu">
        <hr title="Nouvelle page"/>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="bloc[@type_bloc = 'pagination']"/>

    <xsl:template match="bloc[@type_bloc = 'postscriptum']"/>

    <xsl:template mode="show" match="bloc[@type_bloc = 'postscriptum']">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="bloc">
        <xsl:apply-templates/>
    </xsl:template>


    <xsl:template match="paragraphe">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

    <xsl:template match="nouvelle_ligne">
        <!-- si tiret-proustien avant, on recolle la césure de mot donc pas d'espace -->
        <xsl:if test="not(name(preceding-sibling::*[1]) = 'tiret_proustien')">
            <xsl:choose>
                <xsl:when test="name(..) = 'vers'">
                    <br/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text> </xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template match="blanc | alinea | trait | tiret_proustien"/>

    <xsl:template match="ajout">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="code">
        <span class="correction_editeur">
            <xsl:attribute name="title">
                <xsl:text>Forme originale : </xsl:text>
                <xsl:value-of select="."/>
                <xsl:if test="@estime">
                    <xsl:text> - Forme estimée : </xsl:text>
                    <xsl:value-of select="@estime"/>
                </xsl:if>
                <xsl:if test="@estime_par">
                    <xsl:text> - Estimée par : </xsl:text>
                    <xsl:value-of select="@estime_par"/>
                </xsl:if>
            </xsl:attribute>
            <xsl:value-of select="@equivalent"/>
        </span>
    </xsl:template>

    <xsl:template match="surcharge">
        <span class="surcharge">
            <xsl:attribute name="title">
                <xsl:text>Texte surchargé </xsl:text>
                <xsl:if test="@estimation">
                    <xsl:text>estimé : "</xsl:text>
                    <xsl:value-of select="@estimation"/>
                    <xsl:text>"</xsl:text>
                </xsl:if>
                <xsl:if test="@illisible">
                    <xsl:text>illisible</xsl:text>
                </xsl:if>
                <xsl:if test="@ecrit">
                    <xsl:text> : "</xsl:text>
                    <xsl:value-of select="@ecrit"/>
                    <xsl:text>"</xsl:text>
                </xsl:if>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="lieu | lieu_fictif">
        <span>
            <xsl:call-template name="add_default_title_and_class">
                <xsl:with-param name="append_to_title" select="@description_du_lieu"/>
            </xsl:call-template>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="personnage">
        <span>
            <xsl:call-template name="add_default_title_and_class">
                <xsl:with-param name="append_to_title" select="@equivalent"/>
            </xsl:call-template>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="langue_etrangere">
        <mark>
            <xsl:call-template name="add_default_title_and_class">
                <xsl:with-param name="append_to_title" select="@traduction"/>
            </xsl:call-template>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>

    <xsl:template match="oeuvre_fictive">
        <mark>
            <xsl:call-template name="add_default_title_and_class">
                <xsl:with-param name="append_to_title">
                    <xsl:if test="@type">
                        <xsl:text> de type </xsl:text>
                        <xsl:value-of select="@type"/>
                    </xsl:if>
                    <xsl:if test="@equivalent">
                        <xsl:text>. </xsl:text>
                        <xsl:value-of select="@equivalent"/>
                    </xsl:if>
                </xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>

    <xsl:template match="mot_commente">
        <mark>
            <xsl:call-template name="add_default_title_and_class">
                <xsl:with-param name="append_to_title" select="@remarque"/>
            </xsl:call-template>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>

    <xsl:template match="paragraphe_proustien">
        <xsl:text disable-output-escaping="yes">&lt;/p></xsl:text>
        <xsl:text disable-output-escaping="yes">&lt;p></xsl:text>
    </xsl:template>

    <xsl:template match="vers">
        <p>
            <xsl:attribute name="class">vers</xsl:attribute>
            <xsl:apply-templates/>
        </p>
    </xsl:template>

</xsl:stylesheet>
