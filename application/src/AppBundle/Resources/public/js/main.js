$(function() {
  $('[data-toggle="popover"]').popover();
  $('[data-toggle="tooltip"]').tooltip();

  $('body').on('click', '.locale-select', function() {
    var locale = $(this).data("locale");
    var route = Routing.generate('locale_change', {'_locale': locale });

    $.ajax(route).done(function() {
        window.location.reload();
      });
  });

  $( ".person-popover" ).hover(function() {
    var el = $(this);
    utils.getInfoPerson(el);
  });

  var utils = {
    getInfoPerson: function(el){
      if (!el.attr("data-content")) {
        el.attr("data-content", "...");
        el.popover("show");
        var key = el.data("key");
        var url = Routing.generate('info-person', {'key': key });
        fetch(url)
          .then((response) => {
            return response.text();
          })
          .then((text) => {
            $('.person-popover[data-key='+key+']').attr("data-content", text);
            if (el.is(":hover")) {
              el.popover("show");
            }
          });
      }
    }
  }
});
