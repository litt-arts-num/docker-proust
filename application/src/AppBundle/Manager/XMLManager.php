<?php

namespace AppBundle\Manager;

use Symfony\Component\DomCrawler\Crawler;

class XMLManager
{
    public function extract(Crawler $crawler, $tag, $attribute = null)
    {
        $data = $crawler->filter($tag);
        if ($attribute) {
            $data = $data->attr($attribute);
        }

        return $data;
    }

    public function getFullContent($element)
    {
        return $element->ownerDocument->saveHTML($element);
    }

    public function initCrawler($xml)
    {
        $crawler = new Crawler($xml);

        return $crawler;
    }
}
