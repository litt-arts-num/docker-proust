<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Letter;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    /**
     *
     * @Route("/admin/users", name="users_manage", methods={"GET"})
     */
    public function manageUsersAction()
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('user/manage.html.twig', ['users' => $users]);
    }

    /**
     *
     * @Route("/admin/user/{id}/{newRole}", name="user_set_role", methods={"GET"})
     */
    public function setUserRoleAction(User $user, $newRole)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        $em = $this->getDoctrine()->getManager();
        $um = $this->get('fos_user.user_manager');

        $roles = $user->getRoles();
        foreach ($roles as $role) {
          $user->removeRole($role);
        }
        $user->addRole($newRole);
        $um->updateUser($user);

        $this->addFlash('info', $this->get('translator')->trans('user_role_changed'));


        return $this->redirectToRoute('users_manage');
    }

    /**
     * Change locale.
     *
     * @Route("/user/locale/{_locale}/", name="locale_change", methods={"GET"}, options = {"expose" = true})
     */
    public function changeLocaleAction($_locale)
    {
        $this->get('manager.locale')->setUserLocale($_locale);

        return new Response(200);
    }

    /**
     * Toggle letter favorite status
     *
     * @Route("/letter/favorite/{id}", name="favorite_letter_toggle", methods={"GET"})
     * @ParamConverter("letter", class="AppBundle:Letter")
     */
    public function toggleFavoriteAction(Letter $letter)
    {
        $this->get('manager.user')->toggleFavorite($letter);

        return $this->redirectToRoute('letter_proustid', ['proustId' => $letter->getRealId()]);
    }

    /**
     * Display favourite letters
     *
     * @Route("/favorites", name="favorites", methods={"GET"})
     */
    public function displayFavoritesAction()
    {
        return $this->render('user/favorites.html.twig');
    }

    /**
     * Display preference form
     *
     * @Route("/preferences", name="preferences", methods={"GET"})
     */
    public function displayPreferencesAction()
    {
        $form = $this->get('manager.user')->getPreferencesForm();

        return $this->render('user/preferences.html.twig', ['form' => $form]);
    }

    /**
     * Submit preference form
     *
     * @Route("/preferences", name="preferences_save", methods={"POST"})
     */
    public function savePreferencesAction(Request $request)
    {
        $form = $this->get('manager.user')->savePreferencesForm($request);

        return $this->redirectToRoute('preferences');
    }
}
