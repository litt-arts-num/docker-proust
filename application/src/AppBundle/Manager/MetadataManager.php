<?php

namespace AppBundle\Manager;

use AppBundle\Entity\MetadataGroup;
use AppBundle\Entity\MetadataKey;
use AppBundle\Entity\MetadataValue;

class MetadataManager
{
    protected $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function extract($groupName, $metadatas)
    {
        $group = $this->checkGroup($groupName);
        if (!$group) {
            $group = $this->createGroup($groupName);
        }

        foreach ($metadatas as $key => $value) {
            $metadataKey = $this->checkKey($key);
            if (!$metadataKey) {
                $metadataKey = $this->createKey($key, $group);
            }

            if ($value && !$this->checkValue($value, $metadataKey)) {
                $this->createValue($value, $metadataKey);
            }
        }
    }

    public function createKey($name, $group)
    {
        $key = new MetadataKey();
        $key->setName($name);
        $key->setGroup($group);
        $this->em->persist($key);
        $this->em->flush();

        return $key;
    }

    public function createValue($name, $key)
    {
        $value = new MetadataValue();
        $value->setValue($name);
        $value->setKey($key);
        $this->em->persist($value);
        $this->em->flush();

        return $value;
    }

    public function createGroup($name)
    {
        $group = new MetadataGroup();
        $group->setName($name);
        $this->em->persist($group);
        $this->em->flush();

        return $group;
    }

    public function checkKey($name)
    {
        if ($key = $this->em->getRepository('AppBundle:MetadataKey')->findOneByName($name)) {
            return $key;
        }

        return false;
    }

    public function checkGroup($name)
    {
        if ($group = $this->em->getRepository('AppBundle:MetadataGroup')->findOneByName($name)) {
            return $group;
        }

        return false;
    }

    public function checkValue($value, $key)
    {
        if ($this->em->getRepository('AppBundle:MetadataValue')->findBy(['key' => $key, 'value' => $value])) {
            return true;
        }

        return false;
    }

    public function cleanOrphans()
    {
        $metadatas = $this->em->getRepository('AppBundle:MetadataValue')->findAll();

        foreach ($metadatas as $metadata) {
            $key = $metadata->getKey()->getName();
            $value = $metadata->getValue();
            $criterias = array([$key, $value]);
            $letters = $this->em->getRepository('AppBundle:Letter')->getByCriterias($criterias);
            if (count($letters) < 1) {
                $this->em->remove($metadata);
            }
        }

        $this->em->flush();
    }
}
