<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use AppBundle\Form\Type\PersonType;

class PersonController extends Controller
{
    /**
    * Retrieve person info from UIUC API
    *
    * @Route("/infos-person/{key}", name="info-person", methods={"GET"}, options={"expose"=true})
    */
    public function getPersonInfoAction($key)
    {
        $ch = curl_init("https://quest.library.illinois.edu/KP/".$key.".jsonld");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $json = json_decode(curl_exec($ch));
        curl_close($ch);

        return $this->render('person/popover.html.twig', [
          'json' => $json,
        ]);
    }


    /**
     * @Route("admin/persons", name="persons_manage", methods={"GET"})
     */
    public function manageAction()
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        $em = $this->getDoctrine()->getManager();
        $persons = $em->getRepository('AppBundle:Person')->findAll();

        return $this->render('person/manage.html.twig', ["persons" => $persons]);
    }

    /**
     *
     * @Route("admin/edit-person/{id}", name="person_edit", methods={"GET", "POST"})
     */
    public function formAction(Person $person, Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->flush();

            $this->addFlash('info', $this->get('translator')->trans('person_saved'));

            return $this->redirectToRoute('persons_manage');
        }

        return $this->render('person/edit.html.twig', ["form" => $form->createView(), "person" => $person]);
    }
}
