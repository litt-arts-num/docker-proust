<?php

namespace AppBundle\Twig;

use AppBundle\Entity\EditorialContent;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
     $this->em = $em;
    }

    public function getFunctions()
    {
      return [
          new TwigFunction('getContent', [$this, 'getContent']),
      ];
    }

    public function getContent($name)
    {
      $repo = $this->em->getRepository(EditorialContent::class);
      
      return $editorialContent = $repo->findOneBy(["name" => $name]);
    }
}
