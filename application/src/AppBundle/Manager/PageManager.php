<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Letter;
use AppBundle\Entity\Page;

class PageManager
{
    protected $entityManager;
    protected $XMLManager;

    public function __construct($entityManager, $XMLManager)
    {
        $this->entityManager = $entityManager;
        $this->XMLManager = $XMLManager;
    }

    public function create(Letter $letter, $xml)
    {
        $page = new Page();

        $crawler = $this->XMLManager->initCrawler($xml);

        $facsimilePath = $this->XMLManager->extract($crawler, 'fichier_image', 'nom_fichier');
        $externalURL = $this->XMLManager->extract($crawler, 'fichier_image', 'URL');
        
        $page->setLetter($letter);
        $page->setFacsimilePath($facsimilePath);
        $page->setExternalURL($externalURL);

        $this->entityManager->persist($page);

        return $page;
    }
}
