<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Finder\Finder;

class ImportXMLCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('corr-proust:import')
            ->setDescription('import XML')
            ->addArgument('filename', InputArgument::OPTIONAL, 'filename');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>
                  IMPORT XML
        ==============================
        </info>');

        $letterManager = $this->getContainer()->get('manager.letter');

        $finder = new Finder();
        $finder->files()->in(__DIR__.'/../../../var/data')->name('*.xml');

        if ($filename = $input->getArgument('filename')) {
            $finder->name($filename);
        }

        foreach ($finder as $file) {
            $output->writeln('<info>Traitement de <comment>'.$file->getRelativePathname().'</comment></info>');
            $content = $file->getContents();
            $letterManager->importXML($content, true, null, true);
        }

        $output->writeln('<info>
                IMPORT XML TERMINE !
        ==============================
        </info>');
    }
}
