<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Letter;
use AppBundle\Entity\User;
use AppBundle\Form\Type\PreferencesType;
use Symfony\Component\HttpFoundation\Request;

class UserManager
{
    protected $entityManager;
    protected $tokenStorage;
    protected $formFactory;

    public function __construct($entityManager, $tokenStorage, $formFactory)
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->formFactory = $formFactory;
    }

    public function setLocale(User $user, $locale)
    {
        $user->setLocale($locale);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function toggleFavorite(Letter $letter)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if (!$user->getFavoriteLetters()->contains($letter)) {
            $user->addFavoriteLetter($letter);
        } else {
            $user->removeFavoriteLetter($letter);
        }

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return;
    }

    public function getPreferencesForm()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $form = $this->formFactory->createBuilder(PreferencesType::class, $user)->getForm()->createView();

        return $form;
    }

    public function savePreferencesForm(Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        $form = $this->formFactory->createBuilder(PreferencesType::class, $user)->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        }

        return;
    }
}
