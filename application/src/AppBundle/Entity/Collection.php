<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Collection
 *
 * @ORM\Table(name="collection")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CollectionRepository")
 */
class Collection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="nameen", type="string", length=255)
     */
    private $nameEN;

    /**     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**     *
     * @ORM\Column(name="descriptionen", type="text")
     */
    private $descriptionEN;

    /**
     * @ORM\ManyToMany(targetEntity="Letter", inversedBy="collections")
     * @ORM\JoinTable(name="letters_collections")
     * @ORM\OrderBy({"realId" = "DESC"})
     */
    protected $letters;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Collection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Collection
     */
    public function setNameEN($nameEN)
    {
        $this->nameEN = $nameEN;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getNameEN()
    {
        return $this->nameEN;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->letters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add letter.
     *
     * @param \AppBundle\Entity\Letter $letter
     *
     * @return Collection
     */
    public function addLetter(\AppBundle\Entity\Letter $letter)
    {
        $this->letters[] = $letter;

        return $this;
    }

    /**
     * Remove letter.
     *
     * @param \AppBundle\Entity\Letter $letter
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeLetter(\AppBundle\Entity\Letter $letter)
    {
        return $this->letters->removeElement($letter);
    }

    /**
     * Get letters.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLetters()
    {
        return $this->letters;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Collection
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Collection
     */
    public function setDescriptionEN($descriptionEN)
    {
        $this->descriptionEN = $descriptionEN;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescriptionEN()
    {
        return $this->descriptionEN;
    }
}
