<?php

namespace AppBundle\Controller\Edito;

use AppBundle\Entity\Edito\Biography;
use AppBundle\Form\Type\Edito\BiographyType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BiographyController extends Controller
{

  /**
   * Biography form
   *
   * @Route("admin/edito/biography/{id}", name="biography_form", methods={"GET", "POST"}, defaults={"id"=null})
   * @ParamConverter("biography", class="AppBundle:Edito\Biography")
   */
    public function formAction(Biography $biography = null, Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        if (!$biography) {
            $biography = new Biography;
        }

        $form = $this->createForm(BiographyType::class, $biography);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($biography);
            $em->flush();

            $this->addFlash('info', $this->get('translator')->trans('biography_saved'));

            return $this->redirectToRoute('guide', [
            'page' => 'biography',
          ]);
        }

        return $this->render('edito/guide/biography-form.html.twig', [
          'biography' =>  $biography   ,
          'form' => $form->createView(),
       ]);
    }


    /**
     * Biography form
     *
     * @Route("admin/edito/biography/{id}/delete", name="biography_delete", methods={"GET"})
     * @ParamConverter("biography", class="AppBundle:Edito\Biography")
     */
    public function delete(Biography $biography = null)
    {
      if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
          return new Response("Access Denied", 403);
      }
      
        $em = $this->getDoctrine()->getManager();
        $em->remove($biography);
        $em->flush();

        $this->addFlash('info', $this->get('translator')->trans('biography_deleted'));

        return $this->redirectToRoute('guide', [
          'page' => 'biography',
        ]);
    }
}
