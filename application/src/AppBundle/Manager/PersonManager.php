<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Letter;
use AppBundle\Entity\Person;
use Symfony\Component\DomCrawler\Crawler;

class PersonManager
{
    protected $entityManager;
    protected $XMLManager;

    public function __construct($entityManager, $XMLManager)
    {
        $this->entityManager = $entityManager;
        $this->XMLManager = $XMLManager;
    }

    public function findByOrCreateByKey($key, $name, $firstName)
    {
        $person = $this->entityManager->getRepository('AppBundle:Person')->findOneByPersonKey($key);
        if (!$person) {
            $person = $this->create($key, $name, $firstName);
        }

        return $person;
    }

    public function create($key, $name, $firstName)
    {
        $person = new Person();

        $person->setPersonKey($key);
        $name = $this->handleName($name, $firstName);
        $person->setName($firstName.' '.$name);

        $this->entityManager->persist($person);
        $this->entityManager->flush();

        return $person;
    }

    private function handleName($name, $firstName)
    {
        $pattern = '/(.*), (.*)/i';
        $replacement = '$2#$1';
        $name = preg_replace($pattern, $replacement, $name);

        $patterns = ['/([^\']+)#/i', '/([^\']+\')#/i'];
        $replacements = ['$1 ', '$1'];
        $name = preg_replace($patterns, $replacements, $name);

        return $name;
    }

    public function handleXML(Crawler $crawler, Letter $letter)
    {
        $destKey = $this->XMLManager->extract($crawler, 'destinataire', 'cle');
        $expKey = $this->XMLManager->extract($crawler, 'expediteur', 'cle');

        $recipient = $this->findByOrCreateByKey($destKey);
        $sender = $this->findByOrCreateByKey($expKey);

        $letter->setSender($sender);
        $letter->setRecipient($recipient);

        return $letter;
    }
}
