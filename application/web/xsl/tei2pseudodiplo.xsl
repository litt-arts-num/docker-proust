<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
    <!ENTITY carriage_return "&#8617;" >
    <!ENTITY non_breakable_space "&#160;" >
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="1.0">

    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>
    <!-- April 17, 2018 - AnneGF@CNRS - Creation -->
    <!-- June 1, 2018 - AnneGF@CNRS - https://gitlab.com/litt-arts-num/corr-proust/issues/2 -->
    <!-- October 4, 2018 - AnneGF@CNRS - Improving and finish -->
    <!-- October 8, 2018 - AnneGF@CNRS - Cleaning -->

    <!--<xsl:include href="common_tei2.xsl"/>-->

    <xsl:template match="courrier">
        <div class="transcription_pseudodiplomatique">
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:include href="common_tei2transcriptions.xsl"/>

    <!-- Transcription processing for pseudo-diplo view -->
    <xsl:template match="courrier/page/contenu">
        <hr title="Nouvelle page"/>
        <div>
            <xsl:attribute name="class">
                <xsl:text>page_contenu</xsl:text>
                <xsl:text> </xsl:text>
                <xsl:text>col</xsl:text>
                <xsl:call-template name="info_ecriture"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="bloc">
        <div>
            <xsl:attribute name="class">
                <xsl:text>row</xsl:text>
                <xsl:text> </xsl:text>
                <xsl:value-of select="name(.)"/>
                <xsl:call-template name="info_ecriture"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>

    <xsl:template match="paragraphe">
        <!-- Variable useful for layout -->
        <xsl:variable name="margin-left">
            <xsl:choose>
                <xsl:when test="count(//ajout[@position = 'marge_gauche']) > 0">3</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="margin-right">
            <xsl:choose>
                <xsl:when test="count(//ajout[@position = 'marge_droite']) > 0">3</xsl:when>
                <xsl:otherwise>0</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="width_of_paragraph">
            <xsl:value-of select="12 - $margin-left - $margin-right"/>
        </xsl:variable>
        <xsl:variable name="class_for_no_add_in_margin">col col-<xsl:value-of
                select="$width_of_paragraph"/> offset-<xsl:value-of select="$margin-left"/>
        </xsl:variable>
        <xsl:variable name="class_for_margin_left">col order-1 col-<xsl:value-of
                select="$margin-left"/> align-self-center</xsl:variable>
        <xsl:variable name="class_for_margin_right">col order-3 col-<xsl:value-of
                select="$margin-right"/> align-self-center</xsl:variable>
        <xsl:variable name="class_for_has_add_in_margin">col order-2 col-<xsl:value-of
                select="$width_of_paragraph"/></xsl:variable>
        <xsl:if test="$DEV = 1">
            <H5>Variable value</H5>
            <xsl:text>left=</xsl:text>
            <xsl:value-of select="$margin-left"/>
            <br/>
            <xsl:text>right=</xsl:text>
            <xsl:value-of select="$margin-right"/>
            <br/>
            <xsl:text>width=</xsl:text>
            <xsl:value-of select="$width_of_paragraph"/>
            <br/>
            <xsl:text>class-full=</xsl:text>
            <xsl:value-of select="$class_for_no_add_in_margin"/>
            <br/>
            <xsl:text>class-has=</xsl:text>
            <xsl:value-of select="$class_for_has_add_in_margin"/>
            <br/>
            <xsl:text>class-left=</xsl:text>
            <xsl:value-of select="$class_for_margin_left"/>
            <br/>
            <xsl:text>class-right=</xsl:text>
            <xsl:value-of select="$class_for_margin_right"/>
            <xsl:text/>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="count(./ajout[@position = 'marge_gauche']) > 0">
                <!-- do nothing -->
            </xsl:when>
            <xsl:otherwise>
                <div class="col col-12">
                    <div class="row">
                        <p>
                            <xsl:attribute name="class">
                                <xsl:choose>
                                    <xsl:when
                                        test="count(following-sibling::paragraphe[1]/ajout[@position = 'marge_gauche']) + count(following-sibling::paragraphe[1]/ajout[@position = 'marge_droite']) > 0">
                                        <xsl:value-of select="$class_for_has_add_in_margin"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$class_for_no_add_in_margin"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="name(.)"/>
                                <xsl:call-template name="info_ecriture"/>
                            </xsl:attribute>
                            <xsl:if
                                test="(@orientation_texte) and (not(@orientation_texte = ../../bloc/@orientation_texte))">
                                <xsl:comment>COMMENT: Paragraph orientation (<xsl:value-of select="@orientation_texte"/>) different from bloc orientation (<xsl:value-of select="../../bloc/@orientation_texte"/>)</xsl:comment>
                            </xsl:if>
                            <xsl:apply-templates/>
                        </p>
                        <xsl:if
                            test="count(following-sibling::paragraphe[1]/ajout[@position = 'marge_gauche']) > 0">
                            <p>
                                <xsl:attribute name="class">
                                    <xsl:value-of select="$class_for_margin_left"/>
                                </xsl:attribute>
                                <xsl:for-each select="following-sibling::paragraphe[1]">
                                    <xsl:copy>
                                        <xsl:apply-templates/>
                                    </xsl:copy>
                                </xsl:for-each>
                            </p>
                        </xsl:if>
                        <xsl:if
                            test="count(following-sibling::paragraphe[1]/ajout[@position = 'marge_droite']) > 0">
                            <p>
                                <xsl:attribute name="class">
                                    <xsl:value-of select="$class_for_margin_right"/>
                                </xsl:attribute>
                                <xsl:for-each select="following-sibling::paragraphe[1]">
                                    <xsl:copy>
                                        <xsl:apply-templates/>
                                    </xsl:copy>
                                </xsl:for-each>
                            </p>
                        </xsl:if>
                    </div>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="nouvelle_ligne">
        <span class="carriage_return"><xsl:text disable-output-escaping="yes">&#8617;</xsl:text></span><br/>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="ajout">
        <xsl:variable name="marginRight"
            select="format-number(string-length(normalize-space(.)) div $ADD_DIV_FACTOR, '###')"/>
        <span>
            <xsl:attribute name="class">
                <xsl:text>ajout </xsl:text>
                <xsl:text>ajout_</xsl:text>
                <xsl:value-of select="@position"/>
            </xsl:attribute>
            <xsl:attribute name="title">Ajout</xsl:attribute>
            <xsl:choose>
                <xsl:when test="@position = 'interlineaire_superieur'">
                    <xsl:attribute name="style">margin-right: -<xsl:copy-of select="$marginRight"
                        />em;</xsl:attribute>
                </xsl:when>
                <xsl:when test="@position = 'interlineaire_inferieur'">
                    <xsl:attribute name="style">margin-right: <xsl:copy-of select="$marginRight"
                        />em;</xsl:attribute>
                    <xsl:comment>TO CHECK: interlineaire_inferieur</xsl:comment>
                </xsl:when>
                <xsl:when test="@position = 'marge_droite'">
                    <xsl:comment>TODO</xsl:comment>
                </xsl:when>
                <xsl:when test="@position = 'marge_gauche'">
                    <xsl:comment>TODO</xsl:comment>
                </xsl:when>
                <xsl:otherwise/>
            </xsl:choose>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="code">
        <span class="correction_editeur">
            <xsl:attribute name="title">
                <xsl:text>Forme corrigée : </xsl:text>
                <xsl:value-of select="@equivalent"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="surcharge">
        <span class="surcharge">
            <span class="forme">
                <xsl:attribute name="title">
                    <xsl:if test="@estimation">
                        <xsl:text>Lecture supposée / Transcription incertaine</xsl:text>
                    </xsl:if>
                    <xsl:if test="@illisible">
                        <xsl:text>Illisible</xsl:text>
                    </xsl:if>
                </xsl:attribute>
                <xsl:if test="@ecrit">
                    <xsl:value-of select="@ecrit"/>
                </xsl:if>
                <xsl:if test="@estimation">
                    <xsl:value-of select="@estimation"/>
                </xsl:if>
                <xsl:if test="@illisible">
                    <xsl:text>×××</xsl:text>
                </xsl:if>
            </span>
            <span class="surcharge">
                <xsl:apply-templates/>
            </span>
        </span>
    </xsl:template>

    <xsl:template
        match="personnage | oeuvre_fictive | langue_etrangere | mot_commente | paragraphe_proustien | vers">
        <mark>
            <xsl:call-template name="add_default_title_and_class"/>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>
    <xsl:template match="lieu | lieu_fictif | blanc | alinea | tiret_proustien">
        <span>
            <xsl:call-template name="add_default_title_and_class"/>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="trait">
        <!-- longueur à confirmer ; positionnement aussi : display: inline, block, ???-->
        <hr>
            <xsl:attribute name="style"> display:inline-block; </xsl:attribute>
            <xsl:attribute name="width">
                <xsl:choose>
                    <xsl:when test="@taille = 'petit'"> 2em; </xsl:when>
                    <xsl:when test="@taille = 'moyen'"> 4em; </xsl:when>
                    <xsl:when test="@taille = 'grand'"> 8em; </xsl:when>
                </xsl:choose>
            </xsl:attribute>
        </hr>
    </xsl:template>

</xsl:stylesheet>
