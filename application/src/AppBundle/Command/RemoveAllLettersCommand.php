<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Finder\Finder;

class RemoveAllLettersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('corr-proust:delete-all-letters')
            ->setDescription('delete all letters');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');


        $letters = $em->getRepository('AppBundle:Letter')->findAll();

        foreach ($letters as $letter) {
            $em->remove($letter);
            $output->writeln('<info>suppresion de la lettre <comment>'.$letter->getRealId().'</comment></info>');
        }

        $em->flush();
    }
}
