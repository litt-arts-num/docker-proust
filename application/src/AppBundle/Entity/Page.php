<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Page.
 *
 * @ORM\Table(name="page")
 * @ORM\Entity()
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Letter", inversedBy="pages", cascade={"persist"})
     */
    private $letter;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facsimilePath;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $externalURL;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Image()
     */
    private $facsimileFile;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set letter.
     *
     * @param \AppBundle\Entity\Letter $letter
     *
     * @return Page
     */
    public function setLetter(\AppBundle\Entity\Letter $letter = null)
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * Get letter.
     *
     * @return \AppBundle\Entity\Letter
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Set facsimilePath.
     *
     * @param string $facsimilePath
     *
     * @return Page
     */
    public function setFacsimilePath($facsimilePath)
    {
        $this->facsimilePath = $facsimilePath;

        return $this;
    }

    /**
     * Get facsimilePath.
     *
     * @return string
     */
    public function getFacsimilePath()
    {
        return $this->facsimilePath;
    }

    /**
     * Set facsimileFile.
     *
     * @param string $facsimileFile
     *
     * @return Page
     */
    public function setFacsimileFile($facsimileFile)
    {
        $this->facsimileFile = $facsimileFile;

        return $this;
    }

    /**
     * Get facsimileFile.
     *
     * @return string
     */
    public function getFacsimileFile()
    {
        return $this->facsimileFile;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Page
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set externalURL.
     *
     * @param string|null $externalURL
     *
     * @return Page
     */
    public function setExternalURL($externalURL = null)
    {
        $this->externalURL = $externalURL;

        return $this;
    }

    /**
     * Get externalURL.
     *
     * @return string|null
     */
    public function getExternalURL()
    {
        return $this->externalURL;
    }
}
