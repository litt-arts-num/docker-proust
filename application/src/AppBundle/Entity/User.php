<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * Person.
 *
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="locale", type="string", length=2, nullable=true)
     */
    private $locale;

    /**
     * @ORM\ManyToMany(targetEntity="Letter")
     */
    protected $favoriteLetters;

    /**
     * @var string
     * @ORM\Column(name="pref_panel_left", type="string")
     */
    private $leftPanel;

    /**
     * @var string
     * @ORM\Column(name="pref_panel_right", type="string")
     */
    private $rightPanel;

    public function __construct()
    {
        parent::__construct();
        $this->locale = 'fr';
        $this->leftPanel = 'facsimile';
        $this->rightPanel = 'pseudodiplo';
    }

    /**
     * Set locale.
     *
     * @param string $locale
     *
     * @return User
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale.
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Add favoriteLetter.
     *
     * @param \AppBundle\Entity\Letter $favoriteLetter
     *
     * @return User
     */
    public function addFavoriteLetter(\AppBundle\Entity\Letter $favoriteLetter)
    {
        $this->favoriteLetters[] = $favoriteLetter;

        return $this;
    }

    /**
     * Remove favoriteLetter.
     *
     * @param \AppBundle\Entity\Letter $favoriteLetter
     */
    public function removeFavoriteLetter(\AppBundle\Entity\Letter $favoriteLetter)
    {
        $this->favoriteLetters->removeElement($favoriteLetter);
    }

    /**
     * Get favoriteLetters.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoriteLetters()
    {
        return $this->favoriteLetters;
    }

    /**
     * Set leftPanel.
     *
     * @param string $leftPanel
     *
     * @return User
     */
    public function setLeftPanel($leftPanel)
    {
        $this->leftPanel = $leftPanel;

        return $this;
    }

    /**
     * Get leftPanel.
     *
     * @return string
     */
    public function getLeftPanel()
    {
        return $this->leftPanel;
    }

    /**
     * Set rightPanel.
     *
     * @param string $rightPanel
     *
     * @return User
     */
    public function setRightPanel($rightPanel)
    {
        $this->rightPanel = $rightPanel;

        return $this;
    }

    /**
     * Get rightPanel.
     *
     * @return string
     */
    public function getRightPanel()
    {
        return $this->rightPanel;
    }
}
