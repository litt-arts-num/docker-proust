<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * MetadataValue.
 *
 * @ORM\Table(name="metadata_value")
 * @ORM\Entity()
 */
class MetadataValue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="text")
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="MetadataKey", inversedBy="values", cascade={"persist"})
     * @Exclude();
     */
    private $key;

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return MetadataValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set key.
     *
     * @param \AppBundle\Entity\MetadataKey|null $key
     *
     * @return MetadataValue
     */
    public function setKey(\AppBundle\Entity\MetadataKey $key = null)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key.
     *
     * @return \AppBundle\Entity\MetadataKey|null
     */
    public function getKey()
    {
        return $this->key;
    }
}
