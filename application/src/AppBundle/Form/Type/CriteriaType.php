<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\MetadataKey;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class CriteriaType extends AbstractType
{
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('key', EntityType::class, array(
         'class' => 'AppBundle:MetadataKey',
         'choice_label' => 'name',
         'label' => 'criteria',
         'translation_domain' => 'messages',
         'choice_translation_domain' => 'messages',
         'attr' => array('class' => 'form-control metadata-key'),
         'placeholder' => 'choose_option',
         'required' => true,
         'group_by' => function (MetadataKey $metadataKey) {
             return $this->translator->trans($metadataKey->getGroup()->getName());
         },
      ));

        $builder->add('value', EntityType::class, array(
         'class' => 'AppBundle:MetadataValue',
         'choice_label' => 'value',
         'label' => 'value',
         'translation_domain' => 'messages',
         'choice_translation_domain' => 'messages',
         'attr' => array('class' => 'form-control metadata-value'),
         'placeholder' => 'choose_option',
         'required' => true,
      ));
    }
}
