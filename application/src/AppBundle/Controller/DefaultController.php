<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Letter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * Home page
     *
     * @Route("/", name="homepage", methods={"GET"})
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * mentions légales page
     *
     * @Route("/legal", name="legal", methods={"GET"})
     */
    public function legalAction()
    {
        return $this->render('edito/mentions-legales.html.twig');
    }

    /**
     * credits  page
     *
     * @Route("/credits", name="credits", methods={"GET"})
     */
    public function creditsAction()
    {
        return $this->render('edito/credits.html.twig');
    }
}
