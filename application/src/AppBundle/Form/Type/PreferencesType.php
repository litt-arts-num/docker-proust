<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class PreferencesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $choices = [
          'facsimile' => 'facsimile',
          'pseudodiplo' => 'pseudodiplo',
          'linearized' => 'linearized',
          'notes' => 'notes',
          'metadatas' => 'metadatas',
        ];

        $builder->add('leftPanel', ChoiceType::class, array(
            'choices' => $choices,
            'attr' => array('class' => 'form-control'),
            'label' => 'left_panel',
            'translation_domain' => 'messages',
        ));

        $builder->add('rightPanel', ChoiceType::class, array(
            'choices' => $choices,
            'attr' => array('class' => 'form-control'),
            'label' => 'right_panel',
            'translation_domain' => 'messages',
        ));

        $builder->add('save', SubmitType::class, array(
          'attr' => array('class' => 'fa fa-floppy-o btn btn-default btn-dark'),
          'label' => 'save',
          'translation_domain' => 'messages',
        ));
    }

    public function getName()
    {
        return 'preferences';
    }
}
