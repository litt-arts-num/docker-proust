<?php

namespace AppBundle\Entity;

use JMS\Serializer\Annotation\MaxDepth;
use Doctrine\ORM\Mapping as ORM;

/**
 * MetadataGroup.
 *
 * @ORM\Table(name="metadata_group")
 * @ORM\Entity()
 */
class MetadataGroup
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="MetadataKey", mappedBy="group", cascade={"persist"})
     * @MaxDepth(0)
     */
    private $keys;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->keys = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return MetadataGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add key.
     *
     * @param \AppBundle\Entity\MetadataKey $key
     *
     * @return MetadataGroup
     */
    public function addKey(\AppBundle\Entity\MetadataKey $key)
    {
        $this->keys[] = $key;

        return $this;
    }

    /**
     * Remove key.
     *
     * @param \AppBundle\Entity\MetadataKey $key
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeKey(\AppBundle\Entity\MetadataKey $key)
    {
        return $this->keys->removeElement($key);
    }

    /**
     * Get keys.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeys()
    {
        return $this->keys;
    }
}
