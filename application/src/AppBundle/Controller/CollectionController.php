<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Collection;
use AppBundle\Form\Type\CollectionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CollectionController extends Controller
{
    /**
     * @Route("admin/collections", name="collection_manage", methods={"GET"})
     */
    public function manageCollectionAction()
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        $em = $this->getDoctrine()->getManager();
        $collections = $em->getRepository('AppBundle:Collection')->findAll();

        return $this->render('collection/manage.html.twig', ["collections" => $collections]);
    }

    /**
     *
     * @Route("collection/{id}", name="collection_display", methods={"GET"})
     */
    public function displayAction(Collection $collection)
    {
        $em = $this->getDoctrine()->getManager();
        $letters = $em->getRepository('AppBundle:Letter')->findPublishedByCollection($collection);

        return $this->render('collection/display.html.twig', [
          "collection" => $collection,
          "letters" => $letters
        ]);
    }

    /**
     *
     * @Route("admin/collection/{id}/delete", name="collection_delete", methods={"GET"})
     */
    public function deleteAction(Collection $collection)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($collection);
        $em->flush();

        $this->addFlash('info', $this->get('translator')->trans('collection_deleted'));

        return $this->redirectToRoute('collection_manage');
    }

    /**
     *
     * @Route("admin/edit-collection/{id}", name="collection_edit", methods={"GET", "POST"}, defaults={"id"=null})
     * @ParamConverter("collection", class="AppBundle:Collection")
     */
    public function formAction(Collection $collection = null, Request $request)
    {
        if (!$this->container->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return new Response("Access Denied", 403);
        }

        if (!$collection) {
            $collection = new Collection;
        }

        $form = $this->createForm(CollectionType::class, $collection);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($collection);
            $em->flush();

            $this->addFlash('info', $this->get('translator')->trans('collection_saved'));

            return $this->redirectToRoute('collection_manage');
        }

        return $this->render('collection/edit.html.twig', ["form" => $form->createView(), "collection" => $collection]);
    }
}
