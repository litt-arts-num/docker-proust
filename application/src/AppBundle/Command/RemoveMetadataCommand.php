<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Finder\Finder;

class RemoveMetadataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('corr-proust:delete-metadatas')
            ->setDescription('delete all metadatas');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');

        $groups = $em->getRepository('AppBundle:MetadataGroup')->findAll();
        $keys = $em->getRepository('AppBundle:MetadataKey')->findAll();
        $values = $em->getRepository('AppBundle:MetadataValue')->findAll();

        foreach ($values as $value) {
            $em->remove($value);
        }
        $output->writeln('<info>suppresion des values</info>');

        foreach ($keys as $key) {
            $em->remove($key);
        }
        $output->writeln('<info>suppresion des keys</info>');

        foreach ($groups as $group) {
            $em->remove($group);
        }
        $output->writeln('<info>suppresion des groupes</info>');



        $em->flush();
    }
}
