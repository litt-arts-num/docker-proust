<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('xmlFile', FileType::class, array(
            'attr' => array('class' => 'form-control'),
            'label' => 'Fiche XML',
            'required' => true,
        ));

        $builder->add('collections', EntityType::class, [
            'class' => 'AppBundle:Collection',
            'choice_label' => 'name',
            'expanded'  => true,
            'multiple'  => true,
            'label' => 'letter_collections'
        ]);

        $builder->add('save', SubmitType::class, array(
            'attr' => array('class' => 'fa fa-floppy-o btn btn-default btn-dark btn-sm'),
            'label' => ' Envoyer',
            'translation_domain' => 'messages',
        ));
    }

    public function getName()
    {
        return 'letter_upload';
    }
}
