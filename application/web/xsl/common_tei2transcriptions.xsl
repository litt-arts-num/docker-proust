<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
    <!ENTITY carriage_return "&#8617;" >
    <!ENTITY non_breakable_space "&#160;" >

]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:elan="http://elan-numerique.fr"
    exclude-result-prefixes="xs elan" version="1.0">
    <xsl:output method="html" omit-xml-declaration="yes"/>
    <xsl:output indent="yes"/>
    <xsl:strip-space elements="*"/>

    <!-- October 8, 2018 - AnneGF@CNRS - Creation -->

    <xsl:include href="common_tei2.xsl"/>

    <!-- IGNORED -->
    <xsl:template match="courrier/descriptif"/>
    <xsl:template match="courrier/page/fichier_image"/>
    <xsl:template match="note_critique"/>


    <!-- TEMPLATES USED IN OTHER TEMPLATES -->
    <!-- Add class depending on writing information attributes: @outil_ecriture, @type_ecriture, and @orientation_text -->
    <xsl:template name="info_ecriture">
        <xsl:if test="@outil_ecriture">
            <xsl:text> </xsl:text>
            <xsl:value-of select="concat('outil_', @outil_ecriture)"/>
        </xsl:if>
        <xsl:if test="@type_ecriture">
            <xsl:text> </xsl:text>
            <xsl:value-of select="concat('type_', @type_ecriture)"/>
        </xsl:if>
        <xsl:if test="@orientation_texte">
            <xsl:text> </xsl:text>
            <xsl:value-of select="concat('orientation_', @orientation_texte)"/>
        </xsl:if>
        <xsl:if test="@alignement_texte">
            <xsl:text> </xsl:text>
            <xsl:value-of select="concat('alignement_', @alignement_texte)"/>
        </xsl:if>
    </xsl:template>

    <!-- Because no function existes in XSLT 1.0 -->
    <xsl:template name="string-replace-all">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="$text = '' or $replace = '' or not($replace)" >
                <!-- Prevent this routine from hanging -->
                <xsl:value-of select="$text" />
            </xsl:when>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace-all">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- COMMON TEMPLATES FOR PSEUDO-DIPLOMATIC AND LINEARIZED TRANSCRIPTIONS -->
    <xsl:template name="quote_unbreakable_spaces_managment" match="text()">
        <xsl:variable name="text_init">
            <xsl:value-of select="."/>
        </xsl:variable>
        
        <xsl:variable  name="text_zero">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_init" />
                <xsl:with-param name="replace"><xsl:text>
</xsl:text></xsl:with-param>
                <xsl:with-param name="by"> </xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_one_space">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_zero" />
                <xsl:with-param name="replace"><xsl:text>  </xsl:text></xsl:with-param>
                <xsl:with-param name="by"><xsl:text> </xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_oone_space">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_one_space" />
                <xsl:with-param name="replace"><xsl:text>  </xsl:text></xsl:with-param>
                <xsl:with-param name="by"><xsl:text> </xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_ooone_space">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_oone_space" />
                <xsl:with-param name="replace"><xsl:text>  </xsl:text></xsl:with-param>
                <xsl:with-param name="by"><xsl:text> </xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_oooone_space">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_ooone_space" />
                <xsl:with-param name="replace"><xsl:text>  </xsl:text></xsl:with-param>
                <xsl:with-param name="by"><xsl:text> </xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_ooooone_space">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_oooone_space" />
                <xsl:with-param name="replace"><xsl:text>  </xsl:text></xsl:with-param>
                <xsl:with-param name="by"><xsl:text> </xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_one">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_ooooone_space" />
                <xsl:with-param name="replace">«</xsl:with-param>
                <xsl:with-param name="by"><xsl:text disable-output-escaping="no">«&non_breakable_space;</xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_two">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_one" />
                <xsl:with-param name="replace">»</xsl:with-param>
                <xsl:with-param name="by"><xsl:text disable-output-escaping="no">&non_breakable_space;»</xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_three">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_two" />
                <xsl:with-param name="replace">«&non_breakable_space; </xsl:with-param>
                <xsl:with-param name="by"><xsl:text disable-output-escaping="no">«&non_breakable_space;</xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_four">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_three" />
                <xsl:with-param name="replace"> &non_breakable_space;»</xsl:with-param>
                <xsl:with-param name="by"><xsl:text disable-output-escaping="no">&non_breakable_space;»</xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable  name="text_five">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="$text_four" />
                <xsl:with-param name="replace">&non_breakable_space;&non_breakable_space;</xsl:with-param>
                <xsl:with-param name="by"><xsl:text disable-output-escaping="no">&non_breakable_space;</xsl:text></xsl:with-param>
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$text_five"/>
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="renvoi">
        <mark>
            <xsl:attribute name="title">Voir : <xsl:value-of select="@remarque"/></xsl:attribute>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>

    <xsl:template match="rature | plus_gros | plus_petit | lecture_incertaine">
        <mark>
            <xsl:call-template name="add_default_title_and_class"/>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>

    <xsl:template match="citation">
        <mark class="citation">
            <xsl:call-template name="nice-popover">
                <xsl:with-param name="content">
                    <xsl:text>Citation</xsl:text>
                    <xsl:if test="@titre1">
                        <xsl:text> : &lt;i></xsl:text><xsl:value-of select="@titre1"/><xsl:text>&lt;/i></xsl:text>
                    </xsl:if>
                    <xsl:if test="@titre2">
                        <xsl:if test="@titre1"><xsl:text>, </xsl:text></xsl:if>
                        <xsl:text>«&#160;</xsl:text><xsl:value-of select="@titre2"/><xsl:text>&#160;»</xsl:text>
                    </xsl:if>
                    <xsl:if test="@auteur">, <xsl:call-template name="display_author"
                                ><xsl:with-param name="author"><xsl:value-of select="@auteur"
                                /></xsl:with-param></xsl:call-template></xsl:if>
                    <xsl:if test="@date_publication"> (<xsl:value-of select="@date_publication"
                        />)</xsl:if>
                </xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>

    <xsl:template match="oeuvre">
        <mark class="oeuvre">
            <xsl:call-template name="nice-popover">
                <xsl:with-param name="content">
                <xsl:if test="@equivalent_titre1">
                    <xsl:text>&lt;i></xsl:text><xsl:value-of select="@equivalent_titre1"/><xsl:text>&lt;/i></xsl:text>
                </xsl:if>
                <xsl:if test="@equivalent_titre2">
                    <xsl:if test="@equivalent_titre1"><xsl:text>, </xsl:text></xsl:if>
                    <xsl:text>«&#160;</xsl:text><xsl:value-of select="@equivalent_titre2"/><xsl:text>&#160;»</xsl:text>
                </xsl:if>
                <xsl:if test="@type">
                    <xsl:text>, </xsl:text>
                    <!-- artWork -->
                    <xsl:choose>
                        <xsl:when test="@type = 'poesie'">oeuvre poétique</xsl:when>
                        <xsl:when test="@type = 'prose_fiction'">oeuvre</xsl:when>
                        <xsl:when test="@type = 'musique'">oeuvre musicale</xsl:when>
                        <xsl:when test="@type = 'théâtre'">pièce de théâtre/oeuvre dramatique</xsl:when>
                        <xsl:when test="@type = 'arts graphiques'">oeuvre plastique</xsl:when>
                        <xsl:when test="@type = 'article_presse'">article</xsl:when>
                        <xsl:when test="@type = 'prose'">oeuvre</xsl:when>
                        <xsl:when test="@type = 'opéra'">opéra</xsl:when>
                        <xsl:when test="@type = 'sculpture'">sculpture</xsl:when>
                        <xsl:when test="@type = 'revue'">revue</xsl:when>
                        <xsl:when test="@type = 'variétés'">spectacle de variétés</xsl:when>
                        <xsl:when test="@type = 'titre_presse'">périodique</xsl:when>
                    </xsl:choose>
                </xsl:if>
                <xsl:if test="@auteur">
                    <xsl:text> de </xsl:text>
                    <xsl:call-template name="display_author">
                        <xsl:with-param name="author">
                            <xsl:value-of select="@auteur"/>
                        </xsl:with-param>
                    </xsl:call-template>
                </xsl:if>
                <xsl:if test="@date">
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="@date"/>
                    <xsl:text>)</xsl:text>
                </xsl:if>
                </xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>
    
    <xsl:template match="institution">
        <span>
            <xsl:call-template name="add_default_title_and_class">
                <xsl:with-param name="append_to_title" select="@equivalent"/>
            </xsl:call-template>
            <xsl:apply-templates/>
        </span>
    </xsl:template>
    
    
    <xsl:template match="illisible">
        <mark class="illisible">
            <xsl:attribute name="title">
                <xsl:text>Illisible</xsl:text>
            </xsl:attribute>
            <xsl:text>×××</xsl:text>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>

    <xsl:template match="appel_note_critique">
        <sup class="note-call" aria-hidden="true">
            <xsl:attribute name="data-note-id">
                <xsl:value-of select="@identifiant"/>
            </xsl:attribute>
            <xsl:value-of select="substring(@identifiant, 2)"/>
        </sup>
    </xsl:template>

    <xsl:template match="souligne">
        <span>
            <xsl:attribute name="class">
                <xsl:text>souligne souligne_</xsl:text>
                <xsl:value-of select="@type_soulignement"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="date">
        <span class="date">
            <xsl:attribute name="data-toggle">
                <xsl:text>hover</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="data-content">
                <xsl:text>Disabled popover</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="data-date">
                <xsl:value-of select="@date"/>
            </xsl:attribute>
            <!-- Question: doit-on afficher @date d'une façon ou d'une autre et, si oui, voir avec AB comment gère-t-on l'interface multilingue ? -->
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="personne">
        <xsl:choose>
            <xsl:when test="@cle">
                <mark>
                    <xsl:attribute name="class">
                        <xsl:text>person-popover</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="data-trigger">
                        <xsl:text>hover</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="data-html">
                        <xsl:text>true</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="data-toggle">
                        <xsl:text>popover</xsl:text>
                    </xsl:attribute>
                    <xsl:attribute name="data-key">
                        <xsl:value-of select="@cle"/>
                    </xsl:attribute>
                    <xsl:apply-templates/>
                </mark>
            </xsl:when>
            <xsl:otherwise>
                <mark>
                    <xsl:call-template name="add_default_title_and_class">
                        <xsl:with-param name="append_to_title" select="@equivalent"/>
                    </xsl:call-template>
                    <xsl:apply-templates/>
                </mark>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
<!--<xsl:comment>
    <xsl:message>Pour XSL Notes</xsl:message>
    <xsl:template match="lien_externe">
        <a>
            <xsl:attribute name="href"><xsl:value-of select="."/></xsl:attribute>
            LIEN
        </a>
    </xsl:template>
    <xsl:template match="lien_interne">
        <a>
            <xsl:attribute name="href"><xsl:text>/letter/</xsl:text><xsl:value-of select="@proustid"/></xsl:attribute>
        </a>
    </xsl:template>
</xsl:comment>-->
