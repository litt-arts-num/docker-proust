<?php

namespace AppBundle\Controller\Edito;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EditoController extends Controller
{
    /**
     * Display a presentation specific page
     *
     * @Route("/presentation/{page}", name="presentation", methods={"GET"})
     */
    public function presentationAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $out = [];

        switch ($page) {
          case 'updates':
            $out = ['updates' => $em->getRepository('AppBundle:Edito\Update')->findBy([], ['id' => 'DESC'])];
            break;
        }

        return $this->render('edito/presentation/'.$page.'.html.twig', $out);
    }

    /**
     * Display a partner specific page
     *
     * @Route("/partner/{page}", name="partner", methods={"GET"})
     */
    public function partnerAction($page)
    {
        return $this->render('edito/partner/'.$page.'.html.twig');
    }

    /**
     * Display a guides specific page
     *
     * @Route("/guides/{page}", name="guide", methods={"GET"})
     */
    public function guideAction($page)
    {
        $em = $this->getDoctrine()->getManager();
        $out = [];

        switch ($page) {
          case 'biography':
            $out = ['bios' => $em->getRepository('AppBundle:Edito\Biography')->findBy([], ['title'=> 'ASC'])];
            break;

          case 'updates':
            $out = ['updates' => $em->getRepository('AppBundle:Edito\Update')->findAll()];
            break;
        }

        return $this->render('edito/guide/'.$page.'.html.twig', $out);
    }
}
